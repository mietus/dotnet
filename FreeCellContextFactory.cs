﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeCell.Domain.Data
{
    public static class FreeCellContextFactory
    {
        public static FreeCellContext Open()
        {
            var context = new FreeCellContext();
            return context;
        }
    }
}
