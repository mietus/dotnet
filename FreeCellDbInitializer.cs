﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeCell.Domain.Data
{
    public class FreeCellDbInitializer : DropCreateDatabaseIfModelChanges<FreeCellContext>
    {
        protected override void Seed(FreeCellContext entities)
        {
            var users = new List<User>
            {
                new User { Id = 1, Nick = "Mietus", Password = "dwa", Ranking = 0, GryRozegrane = 0,
                    GryWygrane = 0, WirtualnaWaluta = 666 },
                new User { Id = 1, Nick = "Ksalk", Password = "trzy", Ranking = 0, GryRozegrane = 0,
                    GryWygrane = 0, WirtualnaWaluta = 666 }
            };
            
            users.ForEach(s => entities.User.Add(s));
            entities.SaveChanges();
        }
    }
}
