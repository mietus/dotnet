﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Freecell.Common;
using System.ServiceModel.Security;

namespace MegaFreecellWHD
{
    public class CallServiceAsync
    {
        private static MegaFreecellWHD.ServiceReference1.ServiceClient client = new MegaFreecellWHD.ServiceReference1.ServiceClient();
        private static MegaFreecellWHD.ServiceReference2.CreatingUserServiceClient creatingUserClient = new ServiceReference2.CreatingUserServiceClient();

        public static void Call<T>(Func<T> call, Action<T> onSuccess = null, Action<Exception> onException = null)
        {
            Task<T> task = new Task<T>(call);
            if (onSuccess != null)
            {
                task.ContinueWith(t => onSuccess(t.Result), TaskContinuationOptions.OnlyOnRanToCompletion);
            }
            if (onException != null)
            {
                task.ContinueWith(t => onException(t.Exception), TaskContinuationOptions.OnlyOnFaulted);
            }

            task.Start();
        }

        //bool DeleteUserAsync(UserDto dto);

        //GameDto GetNewGameAsync(int count, int seed, int userid);

        public static bool SetUser(string userName,string password)
        {
            client.ClientCredentials.UserName.UserName = userName;
            client.ClientCredentials.UserName.Password = password;
            try
            {
               client.GetUser("");
            }
            catch (MessageSecurityException ex)
            {
                return false;
            }
            return true;
        }

        public static UserDto GetUser(string login)
        {
            return client.GetUser(login);
        }

        public static void PreviewNewGameAsync(int count, int seed, int userid, Action<GameDto> onSuccess = null)
        {
            Call(() => client.PreviewNewGame(count, seed, userid), onSuccess);
        }

        public static void GetNewGameAsync(int count, int seed, int userid, Action<GameDto> onSuccess = null)
        {
            Call(() => client.GetNewGame(count, seed, userid), onSuccess);
        }

        public static void GetDecksRangesAsync(Action<DeckRangeDto[]> onSuccess = null)
        {
            Call(() => client.GetDecksRanges(), onSuccess);
        }

        public static void AddDecksRangesAsync(List<DeckRangeDto> list, Action<bool> onSuccess = null)
        {
            Call(() => client.AddDecksRanges(list.ToArray()), onSuccess);
        }

        public static void DeleteDecksRangesAsync(List<DeckRangeDto> list, Action<bool> onSuccess = null)
        {
            Call(() => client.DeleteDecksRanges(list.ToArray()), onSuccess);
        }

        public static void EndGameAsync(ResultDto dto, Action<bool> onSuccess = null)
        {
            Call(() => client.EndGame(dto), onSuccess);
        }

        public static void FinishUnfinishedGame(int userid, Action<bool> onSuccess = null)
        {
            Call(() => client.FinishUnfinishedGame(userid), onSuccess);
        }

        public static void CreateUserAsync(UserDto dto, Action<bool> onSuccess = null)
        {
            Call(() => creatingUserClient.CreateUser(dto), onSuccess);
        }

        public static void GetUserGames1(int userid, Action<UserDeck1Dto[]> onSuccess = null)
        {
            Call(() => client.GetUserGames1(userid), onSuccess);
        } 

        public static void GetUserGames3(int userid, Action<UserDeck3Dto[]> onSuccess = null)
        {
            Call(() => client.GetUserGames3(userid), onSuccess);
        }

        public static void GetUserGames5(int userid, Action<UserDeck5Dto[]> onSuccess = null)
        {
            Call(() => client.GetUserGames5(userid), onSuccess);
        }

        public static void CreateTournament(TournamentDto dto, Action<bool> onSuccess = null)
        {
            Call(() => client.AddTournament(dto), onSuccess);
        }

        public static void GetTournaments(Action<TournamentDto[]> onSuccess = null)
        {
            Call(() => client.GetTournaments(), onSuccess);
        }

        //List<UserDto> GetUsersAsync();

        //UserDto GetUserAsync(string login);
    }
}
