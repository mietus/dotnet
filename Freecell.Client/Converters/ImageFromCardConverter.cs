﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MegaFreecellWHD.Converters
{
    public class ImageFromCardConverter : IValueConverter
    {
        private List<Brush> images = new List<Brush>();
        private Brush empty = Brushes.DarkOliveGreen;
        public ImageFromCardConverter()
        {

            BitmapImage obrazek = new BitmapImage(new Uri(@"pack://application:,,,/"
             + Assembly.GetExecutingAssembly().GetName().Name
             + ";component/"
             + "Resources/deck.png", UriKind.Absolute)); 
                //new BitmapImage(new Uri("/Freecell.Client;component/Resources/deck.png", UriKind.Relative));
            for (int i = 0; i < 52; i++)
            {
                int wartosc = i / 4;
                Kolor kolor = (Kolor) (i % 4);
                if (kolor == Kolor.Wino)
                    kolor = Kolor.Dzwonek;
                else if (kolor == Kolor.Dzwonek)
                    kolor = Kolor.Wino;
                CroppedBitmap cp = new CroppedBitmap(obrazek, new System.Windows.Int32Rect(wartosc * 73, (int) kolor * 98, 73, 98));
                images.Add( new ImageBrush(cp));
            }
        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Karta k = value as Karta;
            if (k.Nr > -1)
                return images[k.Nr];
            return empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
