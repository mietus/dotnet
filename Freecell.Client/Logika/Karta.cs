﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using Caliburn.Micro;

namespace MegaFreecellWHD
{
    public class Karta : PropertyChangedBase
    {
        public int Nr;
        public int Wartosc { get; private set; }
        public Kolor Kolor { get; private set; }
        private bool zaznaczona;
        public bool Zaznaczona
        {
            get
            {
                return zaznaczona;
            }
            set
            {
                zaznaczona = value;
                NotifyOfPropertyChange(() => Zaznaczona);
            }
        }

        public Polozenie Polozenie { get; set; }

        private static int ZIndex = 0;
        private int getMaxZIndex()
        {
            return ZIndex++;
        }

        public int NrStosu { get; set; }
        public int PozycjaWStosie { get; set; }
        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
                NotifyOfPropertyChange(() => X);
            }
        }
        private int x;

        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
                NotifyOfPropertyChange(() => Y);
            }
        }
        private int y;
        public int Z
        {
            get
            {
                return z;
            }
            set
            {
                z = value;
                NotifyOfPropertyChange(() => Z);
            }
        }
        private int z;
        private Rozdanie rozdanie;

        public int Width { get; set; }
        public int Height { get; set; }

        public static int Szerokosc = 73;
        public static int Wysokosc = 98;

        public static Karta PustaKarta(int x, int y, Rozdanie r)
        {
            Karta karta = new Karta(-1, r);
            karta.X = x;
            karta.Y = y;
            karta.PoprawZIndex();
            karta.NrStosu = 0;
            karta.PozycjaWStosie = 0;
            karta.Polozenie = Polozenie.Ukonczona;

            return karta;
        }

        public Karta(int nr, Rozdanie r)
        {
            rozdanie = r;
            Wartosc = nr /4;
            Kolor = (Kolor) (nr %4);
            if (Kolor == Kolor.Wino)
                Kolor = Kolor.Dzwonek;
            else if (Kolor == Kolor.Dzwonek)
                Kolor = Kolor.Wino;
            
            this.Nr = nr;
        
            // ------------------------
            Width = 65;
            Height = 85;
            // ------------------------

        }

        public bool CzyMoznaPolozycNa(Karta k)
        {
            if (((int)Kolor < 2 && (int)k.Kolor < 2) || ((int)Kolor > 1 && (int)k.Kolor > 1))
                return false;
            if (k.Wartosc == Wartosc + 1)
                return true;
            return false;
        }

        public bool CzyMoznaPolozycNaKoncu(Karta k) 
        {
            if (Kolor == k.Kolor && k.Wartosc + 1 == Wartosc)
                return true;
            return false;
        }

        public void PoprawZIndex()
        {
            Z = getMaxZIndex();
        }

        public void MouseDown(Object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            rozdanie.MouseDown(this, e.GetPosition(null).X, e.GetPosition(null).Y);
        }

        public void MouseUp(Object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            rozdanie.MouseUp(rozdanie, e);
        }
        public void MouseMove(Object sender, MouseEventArgs e)
        {
            e.Handled = true;
            rozdanie.MouseMove(rozdanie, e);
        }
    }

    public enum Polozenie
    {
        Slotowa, Stosowa, Ukonczona
    }

    public enum Kolor
    {
        Zoladz=0,Wino=1,Dzwonek=3,Czerwien=2
    }
}
