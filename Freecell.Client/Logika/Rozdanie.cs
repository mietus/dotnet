﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;
using System.Runtime.InteropServices;
using Caliburn.Micro;
using System.Windows.Threading;
using MegaFreecellWHD.ViewModels;

namespace MegaFreecellWHD
{
    public class Rozdanie : PropertyChangedBase
    {
       
        #region Pola
        public Karta[] Karty { get; set; }
        public Stack<Karta>[] Stosy { get; set; }
        public Stack<Karta>[] Koncowe { get; set; }
        public Karta[] Sloty { get; set; }

        public Stack<Ruch> Ruchy { get; set; }

        private int deltaY;
        private int deltaX;
        private int startY;

        public int Numer { get; set; }

        Dictionary<Kolor, int> mapa = new Dictionary<Kolor, int>(4);

        int ostatnieUproszczenie = 0;
        public int OstatnieUproszczenie
        {
            get
            {
                return ostatnieUproszczenie;
            }
            private set
            {
                ostatnieUproszczenie = value;
                NotifyOfPropertyChange(() => OstatnieUproszczenie);
            }
        }
        private int iloscKlikniec = 0;
        public int IloscKlikniec { 
            get
            {
                return iloscKlikniec;
            }
            private set
            {
                iloscKlikniec = value;
                NotifyOfPropertyChange(() => logikaRef.KliknieciaString);
            }
        }


        private Karta wybranaKarta;
        public Karta WybranaKarta
        {
            get { return wybranaKarta; }
            set 
            {
                if (value != null)
                    value.Zaznaczona = true;
                else if (wybranaKarta != null)
                {
                    wybranaKarta.Zaznaczona = false;
                    doubleClick = false;
                }

                wybranaKarta = value;
                NotifyOfPropertyChange(() => WybranaKarta);
            }
        }

        private int lastX, lastY;

        public List<Karta> wybranyStos = new List<Karta>(52);

        private int iloscWolnychMiejsc = 5;
        public int IloscWolnychMiejsc
        {
            get
            {
                return iloscWolnychMiejsc;
            }
            set
            {
                iloscWolnychMiejsc = value;
                NotifyOfPropertyChange(() => iloscWolnychMiejsc);
            }
        }

        public double posx;
        public double posy;

        private bool czyPrzyciskWcisniety;
        private bool czyKartaWodzi;
        private bool cofanie;

        private bool czySkonczone;
        public bool CzySkonczone
        {
            get { return czySkonczone; }
            set
            {
                czySkonczone = value;
                NotifyOfPropertyChange(() => CzySkonczone);
            }
        }

        private RozgrywkaViewModel logikaRef;

        private bool doubleClick = false;
        private DispatcherTimer timer;

        #endregion

        public void ZmienRozdanie(object sender)
        {
            logikaRef.ZmienRozdanie(sender);
        }

        public Rozdanie(RozgrywkaViewModel logika)
        {
            CzySkonczone = false;
            
            logikaRef = logika;
            deltaY = 25;
            startY = 100;
            deltaX = 100;

            Karty = new Karta[68];
            Stosy = new Stack<Karta>[8];
            Koncowe = new Stack<Karta>[4];
            Sloty = new Karta[4];

            Ruchy = new Stack<Ruch>();

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(500);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();

            Karty = new Karta[52 + 8 + 8];
            for (int i = 0; i < 8; i++)
            {
                Stosy[i] = new Stack<Karta>();

                Karty[52 + i] = Karta.PustaKarta(deltaX + (i % 8) * deltaX, startY,this);
                Karty[60 + i] = Karta.PustaKarta(deltaX + (i % 8) * deltaX, 0,this);
            }
            for (int i = 0; i < 4; i++)
            {
                Koncowe[i] = new Stack<Karta>();
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            doubleClick = false;
            timer.Stop();
        }

        public void generujRozdanie(int[] tablica)
        {
            for (int i = 0; i < 8; i++)
            {
                Stosy[i].Clear();
            }
            for (int i = 0; i < 4; i++)
            {
                Koncowe[i].Clear();
            }

            //int[] tablica = new int[52];
            //int[] tablica = generator(1);
            //int[] tablica = generatorWrapper.generatorWrapper.generujRozdanie(nrDecks);

            for (int i = 0; i < 52; i++)
            {
                Karta k = new Karta(tablica[i], this);
                k.X = deltaX + (i % 8) * deltaX;
                k.Y = Stosy[i % 8].Count == 0 ? startY : Stosy[i % 8].First().Y + deltaY;
                k.PoprawZIndex();
                k.NrStosu = i % 8;
                k.PozycjaWStosie = i / 8;
                Stosy[i % 8].Push(k);
                Karty[i] = k;
                k.Polozenie = Polozenie.Stosowa;
            }
        }


        public void MouseMove(Object sender, MouseEventArgs e)
        {
         
            if (WybranaKarta != null)
            {
                if (czyPrzyciskWcisniety)
                    czyKartaWodzi = true;

                if (!czyKartaWodzi)
                    return;

                double x = e.GetPosition(null).X - posx;
                posx = e.GetPosition(null).X;
                double y = e.GetPosition(null).Y - posy;
                posy = e.GetPosition(null).Y;

                foreach (Karta k in wybranyStos)
                {
                    k.X +=(int) x;
                    k.Y += (int) y;
                }
            }
        }

        public void MouseUp(Object sender, MouseEventArgs e)
        {
            czyPrzyciskWcisniety = false;

            if (!czyKartaWodzi)
                return;

            upuszczanieKarty(sender, (int)e.GetPosition(sender as IInputElement).X, (int)e.GetPosition(sender as IInputElement).Y);
        }

        private void upuszczanieKarty(Object sender, int x, int y)
        {
            if (WybranaKarta == null)
                return;

            if (WybranaKarta != null)
            {
                upusc(x, y);

                czyKartaWodzi = false;
            }

            finalizujRuch();
        }

        private void finalizujRuch()
        {
            uproscGre();

            int ilestosow, ileslotow;
            ilestosow = ileslotow = 0;
            for (int i = 0; i < 8; i++)
                if (Stosy[i].Count == 0)
                    ileslotow++;

            for (int i = 0; i < 4; i++)
                if (Sloty[i] == null)
                    ilestosow++;

            IloscWolnychMiejsc = (ilestosow + 1) * (1 + ileslotow * (ileslotow - 1) / 2);

            WybranaKarta = null;
            wybranyStos.Clear();
        }

        private bool czyTaSama(MouseEventArgs e)
        {
            int x, y;
            x = (int)e.GetPosition(null).X;
            y = (int)e.GetPosition(null).Y;

            if (x > lastX && x < lastX + Karta.Szerokosc && y > lastY && y < lastY + Karta.Wysokosc)
                return true;
            else
                return false;
        }

        private void uproscGre()
        {
            int j = 0;
            bool zmiana = true;
            while (zmiana)
            {
                zmiana = false;
                for (int i = 0; i < 8; i++)
                {
                    if (Stosy[i].Count != 0 && sprobujPolozycNaKoniec(Stosy[i].Peek()))
                    {
                        zmiana = true;
                        Stosy[i].Pop();
                        j++;
                    }
                }
                for (int i = 0; i < 4; i++)
                {
                    if (Sloty[i] != null && sprobujPolozycNaKoniec(Sloty[i]))
                    {
                        zmiana = true;
                        Sloty[i] = null;
                        j++;
                    }
                }
            }
            OstatnieUproszczenie = j;

            foreach (Stack<Karta> s in Koncowe)
                if (s.Count != 13)
                    return;

            CzySkonczone = true;
        }

        private bool sprobujPolozycNaKoniec(Karta k)
        {
            for (int pozycja = 0; pozycja < 4; pozycja++)
                if ((Koncowe[pozycja].Count == 0 && k.Wartosc == 0) //stos pusty a karta jest asem
                    || (Koncowe[pozycja].Count > 0 && k.CzyMoznaPolozycNaKoncu(Koncowe[pozycja].Peek()))) //stos cos zawiera a karte mozna polozyc na wierzch
                {
                    bool kolor = (k.Kolor == Kolor.Czerwien || k.Kolor == Kolor.Dzwonek);
                    if ((kolor && k.Wartosc <= minimumPo(kolor) + 1) //karta jest czerwona a nizszych czarnych nie ma-mozna zdjac
                        || (!kolor && k.Wartosc <= minimumPo(kolor) + 1)) //analogicznie jak wyzej
                    {
                        int staryStos, staraPozycja;
                        Polozenie starePolozenie;
                        staryStos = k.NrStosu;
                        staraPozycja = k.PozycjaWStosie;
                        starePolozenie = k.Polozenie;

                        Koncowe[pozycja].Push(k);                        
                        k.Polozenie = Polozenie.Ukonczona;
                        k.NrStosu = pozycja;
                        k.X = deltaX * (5 + pozycja);
                        k.Y = 0;
                        k.PoprawZIndex();

                        if (!cofanie)
                            Ruchy.Push(new Ruch(staryStos, staraPozycja, k.NrStosu, k.PozycjaWStosie, starePolozenie, k.Polozenie));

                        if (mapa.Count != 4)
                            if (!mapa.ContainsKey(k.Kolor))
                                mapa.Add(k.Kolor, pozycja);
                        return true;
                    }
                }
            return false;
        }

        private int minimumPo(bool kolor)//true dla czerwonego / false dla czarnego
        {
            if (kolor)
            {
                if (!mapa.ContainsKey(Kolor.Wino) || !mapa.ContainsKey(Kolor.Zoladz))
                    return 0;
                int m1=Koncowe[mapa[Kolor.Wino]].Count;
                int m2=Koncowe[mapa[Kolor.Zoladz]].Count;
                return m1 > m2 ? m2 : m1;               
            }
            else
            {
                if (!mapa.ContainsKey(Kolor.Czerwien) || !mapa.ContainsKey(Kolor.Dzwonek))
                    return 0;
                int m1 = Koncowe[mapa[Kolor.Czerwien]].Count;
                int m2 = Koncowe[mapa[Kolor.Dzwonek]].Count;
                return m1 > m2 ? m2 : m1; 
            }
        }

        public void upuscLogicznie(int pozycja, bool czyNaGornymObszarze)
        {
            if (pozycja >= 0 && pozycja < 8 && wybranyStos.Count <= IloscWolnychMiejsc)
            {
                if (!czyNaGornymObszarze //dolna czesc obszaru gry
                    && (Stosy[pozycja].Count == 0 //stos na ktory kladziemy jest pusty
                    || WybranaKarta.CzyMoznaPolozycNa(Stosy[pozycja].Peek()))) //stos na ktory kladziemy akceptuje dana kolumne
                {
                    poloz(pozycja);
                    return;
                }
                if (czyNaGornymObszarze && wybranyStos.Count == 1)
                {
                    if (pozycja < 4 && Sloty[pozycja] == null)//stosy robocze
                    {
                        odlozNaSlot(pozycja);
                        return;
                    }
                    if (pozycja > 3)//stosy koncowe
                        if ((Koncowe[pozycja - 4].Count == 0 //stos jest pusty
                            && WybranaKarta.Wartosc == 0) // karta jest asem
                            || (Koncowe[pozycja - 4].Count > 0 //stos jest niepusty
                            && WybranaKarta.CzyMoznaPolozycNaKoncu(Koncowe[pozycja - 4].Peek()))) //karte mozna polozyc na ostatniej na stosie
                        {
                            odlozNaUkonczone(pozycja);
                            return;
                        }
                }
            }
            odloz();
            return;      
        }

        private void upusc(double x, double y)
        {            
             int pozycja = (int)x / deltaX - 1;
             bool czyNaGornymObszarze = y < startY;
             upuscLogicznie(pozycja, czyNaGornymObszarze);        
        }

        private void odlozNaSlot(int pozycja)
        {
            int staryStos, staraPozycja;
            Polozenie starePolozenie;
            staryStos = WybranaKarta.NrStosu;
            staraPozycja = WybranaKarta.PozycjaWStosie;
            starePolozenie = wybranaKarta.Polozenie;

            Sloty[pozycja] = WybranaKarta;
            WybranaKarta.NrStosu = pozycja;
            WybranaKarta.X = deltaX + pozycja * deltaX;
            WybranaKarta.Y = 0;
            WybranaKarta.Polozenie = Polozenie.Slotowa;
            IloscKlikniec++;

            if (!cofanie)
            Ruchy.Push(new Ruch(staryStos, staraPozycja, WybranaKarta.NrStosu, WybranaKarta.PozycjaWStosie, starePolozenie, WybranaKarta.Polozenie));
        }

        private void odlozNaUkonczone(int pozycja)
        {
            int staryStos, staraPozycja;
            Polozenie starePolozenie;
            staryStos = WybranaKarta.NrStosu;
            staraPozycja = WybranaKarta.PozycjaWStosie;
            starePolozenie = wybranaKarta.Polozenie;

            Koncowe[pozycja - 4].Push(WybranaKarta);
            WybranaKarta.NrStosu = pozycja - 4;
            WybranaKarta.Polozenie = Polozenie.Ukonczona;
            WybranaKarta.X = deltaX * (pozycja + 1);
            WybranaKarta.Y = 0;
            IloscKlikniec++;

            if(!cofanie)
            Ruchy.Push(new Ruch(staryStos, staraPozycja, WybranaKarta.NrStosu, WybranaKarta.PozycjaWStosie, starePolozenie, WybranaKarta.Polozenie));
        }

        private void odloz()
        {
            if ( WybranaKarta.Polozenie == Polozenie.Slotowa)
            {
                WybranaKarta.Y = 0;
                WybranaKarta.X = deltaX + WybranaKarta.NrStosu * deltaX;
            }
            else
                for (int i = wybranyStos.Count - 1; i >= 0; i--)
                {
                    Karta k = wybranyStos[i];
                    Stosy[k.NrStosu].Push(k);
                    k.Y = startY + k.PozycjaWStosie * deltaY;
                    k.X = deltaX + k.NrStosu * deltaX;
                }
        }

        private void poloz(int pozycja)
        {
            int staryStos, staraPozycja;
            Polozenie starePolozenie;
            staryStos = WybranaKarta.NrStosu;
            staraPozycja = WybranaKarta.PozycjaWStosie;
            starePolozenie = wybranaKarta.Polozenie;

            IloscKlikniec++;
            WybranaKarta.Polozenie = Polozenie.Stosowa;
            for (int i = wybranyStos.Count - 1; i >= 0; i--)
            {
                Karta k = wybranyStos[i];
                k.Y = Stosy[pozycja].Count == 0 ? startY : Stosy[pozycja].Peek().Y + deltaY;
                Stosy[pozycja].Push(k);
                k.X = deltaX + pozycja * deltaX;
                k.PoprawZIndex();
                k.NrStosu = pozycja;
                k.PozycjaWStosie = Stosy[pozycja].Count - 1;
            }

            if((starePolozenie != Polozenie.Stosowa || staryStos != WybranaKarta.NrStosu) && !cofanie)
                Ruchy.Push(new Ruch(staryStos, staraPozycja, WybranaKarta.NrStosu, WybranaKarta.PozycjaWStosie, starePolozenie, WybranaKarta.Polozenie));
        }

        public void MouseDown(Karta karta, double x, double y)
        {
            if (doubleClick )
            {
                if (wybranyStos.Count == 1)
                {
                    for (int i = 0; i < 4; ++i)
                        if ((Koncowe[i].Count == 0 //stos jest pusty
                            && WybranaKarta.Wartosc == 0) // karta jest asem
                            || (Koncowe[i].Count > 0 //stos jest niepusty
                            && WybranaKarta.CzyMoznaPolozycNaKoncu(Koncowe[i].Peek())))
                        {
                            odlozNaUkonczone(i + 4);
                            finalizujRuch();
                            return;
                        }
                }

                for (int i = 0; i < 8; ++i)
                {
                    if (WybranaKarta.Polozenie == Polozenie.Stosowa && i == WybranaKarta.NrStosu)
                        continue;

                    if (Stosy[i].Count == 0 || WybranaKarta.CzyMoznaPolozycNa(Stosy[i].Peek()))
                    {
                        poloz(i);
                        finalizujRuch();
                        return;
                    }
                }

                if (wybranyStos.Count == 1)
                {
                    for (int i = 0; i < 4; ++i)
                        if (Sloty[i] == null)
                        {
                            odlozNaSlot(i);
                            finalizujRuch();
                            return;
                        }
                }
            }
            else
            {
                timer.Stop();
                doubleClick = true;
                timer.Start();
            }

            if (wybranaKarta != null)
            {
                upuszczanieKarty(karta, (int)x, (int)y);
                return;
            }

            if (karta.Polozenie == Polozenie.Ukonczona)
                return;

            aktualizujPolozenieKursora(x, y); 
            WybranaKarta = karta;
            lastX = karta.X;
            lastY = karta.Y;

            czyPrzyciskWcisniety = true;

            if (y < startY) //jesli kliknieto sloty
            {
                wybranyStos.Add(karta);
                Sloty[karta.NrStosu] = null;
                WybranaKarta.PoprawZIndex();           
            }
            else if (czyMoznaPodniescKarte())  //jesli kliknieto stosy                 
                    for (int i = wybranyStos.Count - 1; i >= 0; i--)
                        wybranyStos[i].PoprawZIndex();  
            else
              WybranaKarta = null;
        }

        private void aktualizujPolozenieKursora(double x, double y)
        {
            this.posx = x;
            this.posy = y;
        }

        private bool czyMoznaPodniescKarte()
        {
            int nrStosu = WybranaKarta.NrStosu;
            Karta k = null;

            k = Stosy[nrStosu].Pop();
            wybranyStos.Add(k);
            bool dobryStos = true;

            while (k != WybranaKarta && dobryStos)
            {
                k = Stosy[nrStosu].Pop();
               
                if (!wybranyStos.Last().CzyMoznaPolozycNa(k))
                    dobryStos = false; 
  
                wybranyStos.Add(k);
            }

            // podniesc stos mozna, nie mozna upuscic !!!!!!!!!!!!!!!
            //if (!dobryStos || wybranyStos.Count > iloscWolnychMiejsc)
            if (!dobryStos)    
            {
                for (int i = wybranyStos.Count - 1; i >= 0; i--)
                    Stosy[nrStosu].Push(wybranyStos[i]);

                wybranyStos.Clear();
            }

            return dobryStos;
        }


        public void CofnijRuch()
        {
            if (Ruchy.Count == 0)
                return;

            cofanie = true;
            WybranaKarta = null;

            Ruch ruch = Ruchy.Pop();

            //ok
            if (ruch.PolozenieDo == Polozenie.Stosowa && ruch.PolozenieZ == Polozenie.Stosowa)
            {
                WybranaKarta = Stosy[ruch.StosDo].ToList()[Stosy[ruch.StosDo].Count - ruch.PozycjaDo - 1];
                czyMoznaPodniescKarte();
                poloz(ruch.StosZ);
            }

            //ok
            if (ruch.PolozenieDo == Polozenie.Stosowa && ruch.PolozenieZ == Polozenie.Slotowa)
            {
                WybranaKarta = Stosy[ruch.StosDo].Pop();
                Sloty[ruch.StosZ] = WybranaKarta;
                WybranaKarta.NrStosu = ruch.StosZ;
                WybranaKarta.X = deltaX + ruch.StosZ * deltaX;
                WybranaKarta.Y = 0;
                WybranaKarta.Polozenie = Polozenie.Slotowa;
            }

            //ok
            if (ruch.PolozenieDo == Polozenie.Slotowa && ruch.PolozenieZ == Polozenie.Stosowa)
            {
                WybranaKarta = Sloty[ruch.StosDo];
                Sloty[ruch.StosDo] = null;
                wybranyStos.Add(WybranaKarta);
                poloz(ruch.StosZ);
            }

            //ok
            if (ruch.PolozenieDo == Polozenie.Ukonczona && ruch.PolozenieZ == Polozenie.Slotowa)
            {
                WybranaKarta = Sloty[ruch.StosZ];
                Sloty[ruch.StosZ] = null;
                WybranaKarta.NrStosu = ruch.StosZ;
                WybranaKarta.X = deltaX + (ruch.StosZ + 4) * deltaX;
                WybranaKarta.Y = 0;
                WybranaKarta.Polozenie = Polozenie.Ukonczona;
            }

            //ok
            if (ruch.PolozenieDo == Polozenie.Ukonczona && ruch.PolozenieZ == Polozenie.Stosowa)
            {
                WybranaKarta = Koncowe[ruch.StosDo].Pop();
                Stosy[ruch.StosZ].Push(WybranaKarta);

                WybranaKarta.NrStosu = ruch.StosZ;
                WybranaKarta.X = deltaX + ruch.StosZ * deltaX;
                WybranaKarta.Y = startY + ruch.PozycjaZ * deltaY;
                WybranaKarta.Polozenie = Polozenie.Stosowa;
            }
            
            WybranaKarta = null;
            wybranyStos.Clear();
            cofanie = false;
        }


    }
}
