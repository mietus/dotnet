﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MegaFreecellWHD
{
    public class Ruch
    {
        public int StosZ { get; set; }
        public int PozycjaZ { get; set; }
        public Polozenie PolozenieZ { get; set; }

        public int StosDo { get; set; }
        public int PozycjaDo { get; set; }
        public Polozenie PolozenieDo { get; set; }

        public Ruch(int stosZ, int pozycjaZ, int stosDo, int pozycjaDo, Polozenie polozenieZ, Polozenie polozenieDo)
        {
            StosZ = stosZ;
            PozycjaZ = pozycjaZ;
            StosDo = stosDo;
            PozycjaDo = pozycjaDo;
            PolozenieZ = polozenieZ;
            PolozenieDo = polozenieDo;

            Console.WriteLine(this);
        }

        public override string ToString()
        {
            string s = "";
            s += "(" + StosZ.ToString() + "," + PozycjaZ.ToString()  + "," + PolozenieZ.ToString() +  ")";
            s += " -> ";
            s += "(" + StosDo.ToString() + "," + PozycjaDo.ToString() + "," + PolozenieDo.ToString() + ")";
            return s;
        }
    }
}
