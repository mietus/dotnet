﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace MegaFreecellWHD.Logika
{
    public class Wynik : PropertyChangedBase
    {
        private int najlepszyCzas;
        public int NajlepszyCzas
        {
            get { return najlepszyCzas; }
            set
            {
                najlepszyCzas = value;
                NotifyOfPropertyChange(() => NajlepszyCzasString);
            }
        }

        public string NajlepszyCzasString
        {
            get { return NajlepszyCzas == 0 ? "0" : NajlepszyCzas.ToString(); }
        }

        private int najlepszaIloscKlikniec;
        public int NajlepszaIloscKlikniec
        {
            get { return najlepszaIloscKlikniec; }
            set
            {
                najlepszaIloscKlikniec = value;
                NotifyOfPropertyChange(() => NajlepszaIloscKlikniecString);
            }
        }

        public string NajlepszaIloscKlikniecString
        {
            get { return NajlepszaIloscKlikniec == 0 ? "0" : NajlepszaIloscKlikniec.ToString(); }
        }

        private int sredniCzas;
        public int SredniCzas
        {
            get { return sredniCzas; }
            set
            {
                sredniCzas = value;
                NotifyOfPropertyChange(() => SredniCzasString);
            }
        }

        public string SredniCzasString
        {
            get { return SredniCzas == 0 ? "0" : SredniCzas.ToString(); }
        }

        private int sredniaIloscKlikniec;
        public int SredniaIloscKlikniec
        {
            get { return sredniaIloscKlikniec; }
            set
            {
                sredniaIloscKlikniec = value;
                NotifyOfPropertyChange(() => SredniaIloscKlikniecString);
            }
        }

        public string SredniaIloscKlikniecString
        {
            get { return SredniaIloscKlikniec == 0 ? "0" : SredniaIloscKlikniec.ToString(); }
        }
    }
}
