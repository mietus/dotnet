﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace Freecell.Client
{
    public class MyX509Validator : X509CertificateValidator
    {
        public override void Validate(
                X509Certificate2 certificate)
        {
            if (certificate == null ||
              certificate.SubjectName.Name != "CN=MyServerCert")
            {
                throw new SecurityTokenValidationException(
                  "Certificate validation error");
            }
        } 
    }
}