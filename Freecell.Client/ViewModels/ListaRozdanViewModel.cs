﻿namespace MegaFreecellWHD.ViewModels
{
    using Caliburn.Micro;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Serialization;
    using System.IO;
    using MegaFreecellWHD.Views;
    using Freecell.Common;

    public class ListaRozdanViewModel : Screen
    {
        internal static ListaRozdanViewModel Instancja
        {
            get
            {
                return instancja;
            }
        }
        
        private static ListaRozdanViewModel instancja = new ListaRozdanViewModel();
        private ListaRozdanViewModel()
        {
            CallServiceAsync.GetDecksRangesAsync(list =>
                {
                    Zakresy = new ObservableCollection<DeckRangeDto>(list);
                });
        }

        private ObservableCollection<DeckRangeDto> zakresy = new ObservableCollection<DeckRangeDto>();

        public ObservableCollection<DeckRangeDto> Zakresy
        {
            get { return zakresy; }
            set
            {
                zakresy = value;
                NotifyOfPropertyChange(() => Zakresy);
            }
        }

        private List<DeckRangeDto> usuniete = new List<DeckRangeDto>();
        private List<DeckRangeDto> dodane = new List<DeckRangeDto>();

        private Random random = new Random();

        private DeckRangeDto selectedItem;
        public DeckRangeDto SelectedItem
        {
            get { return selectedItem; }
            set { selectedItem = value; }
        }
        

        public void Add(DeckRangeDto zr)
        {
            Zakresy.Add(zr);
            dodane.Add(zr);
            NotifyOfPropertyChange(() => Zakresy);
        }

        public void Remove()
        {
            if (SelectedItem == null)
                return;

            usuniete.Add(SelectedItem);
            Zakresy.Remove(SelectedItem);
            NotifyOfPropertyChange(() => Zakresy);
        }

        public void Add()
        {
            int from, to;
            if (!int.TryParse(From, out from))
                return;
            if (!int.TryParse(To, out to))
                return;
            if (from > to || from<0 || to<0)
                return;
            Add(new DeckRangeDto() {To=to,From=from,Id=-1});
        }

        public string From
        { get; set; }
        public string To
        { get; set; }

        public void OK()
        {
            if(dodane.Count > 0)
                CallServiceAsync.AddDecksRangesAsync(dodane);
            if(usuniete.Count > 0)
                CallServiceAsync.DeleteDecksRangesAsync(usuniete);
            TryClose(true);
        }

        public void Anuluj()
        {
            foreach (DeckRangeDto z in dodane)
                Zakresy.Remove(z);

            foreach (DeckRangeDto z in usuniete)
                Zakresy.Add(z);

            TryClose(false);
        }
    }

    
  
    //public class ZakresRozdan : PropertyChangedBase
    //{
    //    public ZakresRozdan()
    //    {

    //    }

    //    public void Remove()
    //    {
    //        ListaRozdanViewModel.Instancja.Remove(this);
    //    }

    //    internal ZakresRozdan(int from, int to)
    //    {
    //        From = from;
    //        To = to;
    //    }

        
    //    private int from;
    //    public int From
    //    {
    //        get { return from; }
    //        set
    //        {
    //            from = value;
    //            NotifyOfPropertyChange(() => From);
    //        }
    //    }

    //    private int to;
    //    public int To
    //    {
    //        get { return to; }
    //        set
    //        {
    //            to = value;
    //            NotifyOfPropertyChange(() => To);
    //        }
    //    }

    //    public override string ToString()
    //    {
    //        if (from == to)
    //            return from.ToString();
    //        return from + " - " + to;
    //    }
    //}
}
