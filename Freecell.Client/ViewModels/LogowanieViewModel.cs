﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;
using Freecell.Common;
using MegaFreecellWHD.ServiceReference1;

namespace MegaFreecellWHD.ViewModels
{
    [Export(typeof(LogowanieViewModel))]
    public class LogowanieViewModel : Screen
    {
        IWindowManager windowManager;

        private string login;
        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        private string haslo;
        public string Haslo
        {
            get { return haslo; }
            set { haslo = value; }
        }

        [ImportingConstructor]
        public LogowanieViewModel(IWindowManager windowManager)
        {
            this.windowManager = windowManager;
        }
        
        public void Loguj()
        {
            if (CallServiceAsync.SetUser(Login, Haslo))
            {
                var u = CallServiceAsync.GetUser(Login);
                CallServiceAsync.FinishUnfinishedGame(u.Id);
                windowManager.ShowWindow(new RozgrywkaViewModel(windowManager, u));
                new Task(() => TryClose()).Start();
            }
            else
            {
                MessageBox.Show("Zly user/haslo");
            }
        }

        public void StworzKonto()
        {
            windowManager.ShowDialog(new StworzKontoViewModel(windowManager));
        }

        public void Anuluj()
        {
            TryClose();
        }
    }
}
