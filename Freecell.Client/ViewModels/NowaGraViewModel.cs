﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using MegaFreecellWHD.Views;

namespace MegaFreecellWHD.ViewModels
{
    public class NowaGraViewModel : Screen
    {
        private static NowaGraViewModel instance = new NowaGraViewModel();
             
        public static NowaGraViewModel GetInstance()
        {
            return instance;
        }

        public int Seed { get; set; }

        private int ileRozdan;
        public int NumberDecks
        {
            get { return ileRozdan; }
            set
            {
                ileRozdan = value;
                NotifyOfPropertyChange(() => NumberDecks);
            }
        }

        private int czasGry;
        public int CzasGry
        {
            get { return czasGry; }
            set
            {
                czasGry = value;
                NotifyOfPropertyChange(() => CzasGry);
            }
        }

        public bool Limit { get; set; }

        private int? clickLimit;
        public int? ClickLimit
        {
            get
            {
                    return clickLimit;
            }
            set 
            { 
                clickLimit = value;
                NotifyOfPropertyChange(() => ClickLimit);
            }
        }

        public List<int[]> Decks { get; set; }

        public int Id { get; set; }

        private int trudnosc;

        public string Difficulty
        {
            get
            {
                if (trudnosc < 200)
                    return "Łatwe";
                if (trudnosc < 400)
                    return "Średnie";
                return "Trudne";
            }
        }

        public int DifficultyInt
        {
            set { trudnosc = value;
            NotifyOfPropertyChange(() => Difficulty);
            }
            get
            {
                return trudnosc;
            }
           
        }
        

        public void UstawIloscRozdan(int ile)
        {
            NumberDecks = ile;
            Seed = (new Random()).Next();
            CallServiceAsync.PreviewNewGameAsync(ile, Seed, 1, graDto =>
            {
                CzasGry = graDto.TimeLimit;
                ClickLimit = graDto.ClickLimit;
                DifficultyInt = graDto.Difficulty;
            });
        }

        public void OK()
        {
            CallServiceAsync.GetNewGameAsync(NumberDecks, Seed, 1, graDto =>
            {
                CzasGry = graDto.TimeLimit;
                ClickLimit = graDto.ClickLimit;
                DifficultyInt = graDto.Difficulty;
                Decks = new List<int[]>(graDto.Decks);
                Id = graDto.Id;
                TryClose(true);
            });
        }

        public void Anuluj()
        {
            TryClose(false);
        }
    }
}
