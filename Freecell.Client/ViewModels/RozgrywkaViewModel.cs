﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using Freecell.Common;
using MegaFreecellWHD;

namespace MegaFreecellWHD.ViewModels
{
    public class RozgrywkaViewModel : PropertyChangedBase
    {
        private UserDto user { get; set; }
        IWindowManager windowManager;

        public Rozdanie WybraneRozdanie
        {
            get
            {
                return wybraneRozdanie;
            }
            set
            {
                wybraneRozdanie = value;
                NotifyOfPropertyChange(() => WybraneRozdanie);
            }
        }
        private Rozdanie wybraneRozdanie;

        public ObservableCollection<Rozdanie> Decks
        {
            get { return rozdania; }
            set
            {
                rozdania = value;
                NotifyOfPropertyChange(() => Decks);
            }
        }
        private ObservableCollection<Rozdanie> rozdania;

        public int GierWygranych
        {
            get;
            set;
        }

        public int GierPrzegranych
        {
            get;
            set;
        }

        public string KliknieciaString
        {
            get
            {
                return SumaKlikniec.ToString() + (ClickLimit == null ? "" : ("/" + ClickLimit.ToString()));
            }
        }

        public string CzasString
        {
            get
            {
                return CzasGry + "/" + LimitCzasu;
            }
        }

        public int GierRozegranych
        {
            get
            {
                return GierPrzegranych + GierWygranych;
            }
        }

        public int CzasGry
        {
            get
            {
                if (startTime != null)
                    return (int)(DateTime.Now - startTime).TotalSeconds;
                else
                    return 0;
            }
        }

        public bool IsAdmin
        {
            get
            {
                return user.Admin == 1;
            }
        }

        public int SumaKlikniec
        {
            get
            {
                int suma = 0;
                foreach (Rozdanie r in Decks)
                    suma += r.IloscKlikniec;
                return suma;
            }
        }

        public int LimitCzasu { get; set; }
        public int? ClickLimit { get; set; }
        private DateTime startTime { get; set; }
        private DispatcherTimer timer;

        private NowaGraViewModel aktualnaGra
        {
            get { return NowaGraViewModel.GetInstance(); }
        }

        public RozgrywkaViewModel(IWindowManager windowManager, UserDto user)
        {
            this.windowManager = windowManager;
            this.user = user;
            Decks = new ObservableCollection<Rozdanie>();
            startTime = DateTime.Now;
        }


        private void timer_Tick(object sender, EventArgs e)
        {
            NotifyOfPropertyChange(() => CzasString);
            NotifyOfPropertyChange(() => KliknieciaString);

            if (CzasGry > LimitCzasu)
            {
                timer.Stop();
                MessageBox.Show("Przekoczono limit czasu!");
                WybraneRozdanie = null;
                Decks.Clear();
                return;
            }

            if(ClickLimit != null && SumaKlikniec > ClickLimit)
            {
                timer.Stop();
                MessageBox.Show("Przekoczono limit klikniec!");
                WybraneRozdanie = null;
                Decks.Clear();
                return;
            }

            foreach (Rozdanie r in Decks)
            {
                if (r.CzySkonczone == false)
                    return;
            }

            timer.Stop();
            MessageBox.Show("Gratulacje! Ukonczyles rozdania!\nCzas: " + CzasGry + " Klikniecia: " + SumaKlikniec);
            timer.Stop();
        }

        public void WygrajGre()
        {
            zakonczGre(true);
        }

        public void WygrajRozdanie()
        {
            WybraneRozdanie.CzySkonczone = true;
        }

        private void zakonczGre(bool wynik)
        {
            ResultDto res = new ResultDto();
            res.Result = wynik ? obliczWynik() : -aktualnaGra.DifficultyInt;
            res.Time = CzasGry;
            res.ClickNumber = SumaKlikniec;
            res.Id = aktualnaGra.Id;
            res.DeckNumber = Decks.Count;

            CallServiceAsync.EndGameAsync(res);
        }

        private int obliczWynik()
        {
            double T, K, O;
            int trudnosc = aktualnaGra.DifficultyInt;
            T = 1 - (double)CzasGry / LimitCzasu;
            K = 1 - (double)SumaKlikniec / (int)ClickLimit;

            int sumakart = 0;
            foreach(Rozdanie r in Decks) sumakart += r.OstatnieUproszczenie;

            O = 1 - (double)sumakart / (52 * Decks.Count);

            return (int)(trudnosc * (T + K + O));
        }

        public void StworzDecks(List<int[]> lista)
        {
            Decks.Clear();

            for (int i = 0; i < lista.Count ; ++i)
            {
                Rozdanie r = new Rozdanie(this);
                r.generujRozdanie(lista[i]);
                r.Numer = i;
                Decks.Add(r);
            }            
        }

        public void CofnijRuch()
        {
            if(WybraneRozdanie != null)
                WybraneRozdanie.CofnijRuch();
        }

        public void ZmienRozdanie(object sender)
        {
            WybraneRozdanie = sender as Rozdanie;
        }


        public void EdytujZakresy()
        {
            ListaRozdanViewModel lr = ListaRozdanViewModel.Instancja;
            windowManager.ShowDialog(lr);
        }

        public void NowaGra()
        {
            if (windowManager.ShowDialog(aktualnaGra) != true)
                return;

            StworzDecks(aktualnaGra.Decks);
            WybraneRozdanie = Decks[0];
            LimitCzasu = aktualnaGra.CzasGry;
            ClickLimit = aktualnaGra.ClickLimit;
            

            startTime = DateTime.Now;
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(500);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        public void ZakonczGre()
        {
            Application.Current.Shutdown();
        }

        public void PokazWyniki()
        {
            windowManager.ShowDialog(new WynikiViewModel(windowManager, user.Id));
        }

        public void PokazListeTurniejow()
        {
            windowManager.ShowDialog(new TurniejeViewModel(windowManager));
        }

        public void StworzTurniej()
        {
            windowManager.ShowDialog(new StworzTurniejViewModel(windowManager, user.Id));
        }
    }


}
