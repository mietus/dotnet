﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Freecell.Common;

namespace MegaFreecellWHD.ViewModels
{
    public class StworzKontoViewModel : Screen
    {
        IWindowManager windowManager;

        private string login;

        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }     
        
        public StworzKontoViewModel(IWindowManager windowManager)
        {
            this.windowManager = windowManager;
        }

        public void StworzKonto()
        {
            UserDto dto = new UserDto() { Nick = Login, GamesPlayed = 0, GamesWon = 0, Password = Password, Ranking = 0, Currency = 0 };

            //CallServiceAsync.CreateUser(dto);
            CallServiceAsync.CreateUserAsync(dto);
            TryClose();
        }

        public void Anuluj()
        {
            TryClose();
        }
    }
}
