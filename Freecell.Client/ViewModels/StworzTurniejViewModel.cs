﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Freecell.Common;

namespace MegaFreecellWHD.ViewModels
{
    public class StworzTurniejViewModel : Screen
    {
        IWindowManager windowManager;
        private int userId;

        private int wpisowe;
        public int EntryFee
        {
            get { return wpisowe; }
            set { wpisowe = value; }
        }

        private int ileRozdan;

        public int NumberDecks
        {
            get { return ileRozdan; }
            set { ileRozdan = value; }
        }

        private string nazwaTurnieju;

        public string TournamentName
        {
            get { return nazwaTurnieju; }
            set { nazwaTurnieju = value; }
        }

        private int czasTrwania;

        public int CzasTrwania
        {
            get { return czasTrwania; }
            set { czasTrwania = value; }
        }
        


        public StworzTurniejViewModel(IWindowManager windowManager, int userId)
        {
            this.windowManager = windowManager;
            this.userId = userId;
        }

        public void StworzTurniej()
        {
            TournamentDto dto = new TournamentDto() { NumberDecks = NumberDecks, EntryFee = EntryFee, BeginingDate = DateTime.Now, EndingDate = DateTime.Now + TimeSpan.FromMinutes(CzasTrwania), Name = TournamentName, Seed = (new Random()).Next(10000), FounderId = userId, WinnerId = userId };

            CallServiceAsync.CreateTournament(dto);
            TryClose(true);
        }

        public void Anuluj()
        {
            TryClose(false);
        }

    }
}
