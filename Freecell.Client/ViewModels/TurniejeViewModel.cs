﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Freecell.Common;

namespace MegaFreecellWHD.ViewModels
{
    public class TurniejeViewModel : Screen
    {
        IWindowManager windowManager;

        private List<TournamentDto> turniejeDto;

        private BindableCollection<string> turnieje;

        public BindableCollection<string> Turnieje
        {
            get { return turnieje; }
            set
            {
                turnieje = value;
                NotifyOfPropertyChange(() => Turnieje);
            }
        }

        private string nazwaTurnieju;
        public string TournamentName
        {
            get { return nazwaTurnieju; }
            set
            {
                nazwaTurnieju = value;
                NotifyOfPropertyChange(() => TournamentName);
            }
        }

        private int wpisowe;
        public int EntryFee
        {
            get { return wpisowe; }
            set
            {
                wpisowe = value;
                NotifyOfPropertyChange(() => EntryFee);
            }
        }

        private DateTime dataRozpoczecia;
        public DateTime BeginingDate
        {
            get { return dataRozpoczecia; }
            set
            {
                dataRozpoczecia = value;
                NotifyOfPropertyChange(() => BeginingDate);
            }
        }

        private DateTime dataZakonczenia;
        public DateTime EndingDate
        {
            get { return dataZakonczenia; }
            set
            {
                dataZakonczenia = value;
                NotifyOfPropertyChange(() => EndingDate);
            }
        }

        private int ileRozdan;
        public int NumberDecks
        {
            get { return ileRozdan; }
            set
            {
                ileRozdan = value;
                NotifyOfPropertyChange(() => NumberDecks);
            }
        }

        private int zalozyciel;
        public int Zalozyciel
        {
            get { return zalozyciel; }
            set
            {
                zalozyciel = value;
                NotifyOfPropertyChange(() => Zalozyciel);
            }
        }

        public TurniejeViewModel(IWindowManager windowManager)
        {
            this.windowManager = windowManager;

            Inicjalizuj();
        }

        private void Inicjalizuj()
        {
            Turnieje = new BindableCollection<string>();
            turniejeDto = new List<TournamentDto>();

            CallServiceAsync.GetTournaments(delegate(TournamentDto[] dtos)
            {
                turniejeDto = dtos.ToList();
                foreach (TournamentDto dto in turniejeDto)
                {
                    Turnieje.Add(dto.Name);
                }
            });
        }

        public void PokazTurniej(object sender)
        {
            int index = (int)sender;

            TournamentName = turniejeDto[index].Name;
            EntryFee = turniejeDto[index].EntryFee;
            BeginingDate = turniejeDto[index].BeginingDate;
            EndingDate = turniejeDto[index].EndingDate;
            NumberDecks = turniejeDto[index].NumberDecks;
            Zalozyciel = turniejeDto[index].FounderId;
        }
    }
}
