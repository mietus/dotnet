﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Caliburn.Micro;
using Freecell.Common;
using MegaFreecellWHD.Logika;

namespace MegaFreecellWHD.ViewModels
{
    public class WynikiViewModel : Screen
    {
        IWindowManager windowManager;
        private int userId;

        private List<UserDeck1Dto> rozdania1;
        public List<UserDeck1Dto> Decks1
        {
            get { return rozdania1; }
            set { rozdania1 = value; }
        }

        private List<UserDeck3Dto> rozdania3;
        public List<UserDeck3Dto> Decks3
        {
            get { return rozdania3; }
            set { rozdania3 = value; }
        }

        private List<UserDeck5Dto> rozdania5;
        public List<UserDeck5Dto> Decks5
        {
            get { return rozdania5; }
            set { rozdania5 = value; }
        }

        private List<string> wyniki;
        public List<string> Wyniki
        {
            get { return wyniki; }
            set { wyniki = value; }
        }

        private BindableCollection<string> idRozdan;
        public BindableCollection<string> IdRozdan
        {
            get { return idRozdan; }
            set
            {
                idRozdan = value;
                NotifyOfPropertyChange(() => IdRozdan);
            }
        }

        private Wynik wynik;
        public Wynik Wynik
        {
            get { return wynik; }
            set { wynik = value; }
        }

        private int count = 1;

        public WynikiViewModel(IWindowManager windowManager, int userId)
        {
            this.windowManager = windowManager;
            this.userId = userId;

            Inicjalizuj();
        }

        private void Inicjalizuj()
        {
            IdRozdan = new BindableCollection<string>();
            Wynik = new Wynik();

            CallServiceAsync.GetUserGames1(userId, delegate(UserDeck1Dto[] dtos)
            {
                Decks1 = dtos.ToList();
            });

            CallServiceAsync.GetUserGames3(userId, delegate(UserDeck3Dto[] dtos)
            {
                Decks3 = dtos.ToList();
            });

            CallServiceAsync.GetUserGames5(userId, delegate(UserDeck5Dto[] dtos)
            {
                Decks5 = dtos.ToList();
            });
        }

        public void ZmienIloscRozdan(object sender)
        {
            count = (int)sender * 2 + 1;

            if (count == 1)
            {
                IdRozdan.Clear();
                if (Decks1 != null)
                    foreach (UserDeck1Dto dto in Decks1)
                    {
                        string temp = dto.DeckId1.ToString();
                        if (!IdRozdan.Contains(temp))
                            IdRozdan.Add(temp);
                    }
            }
            else if (count == 3)
            {
                IdRozdan.Clear();
                if (Decks3 != null)
                    foreach (UserDeck3Dto dto in Decks3)
                    {
                        string temp = dto.DeckId1.ToString() + " - " + dto.DeckId2.ToString() + " - " + dto.DeckId3.ToString();
                        if (!IdRozdan.Contains(temp))
                            IdRozdan.Add(temp);
                    }
            }
            else
            {
                IdRozdan.Clear();
                if (Decks5 != null)
                    foreach (UserDeck5Dto dto in Decks5)
                    {
                        string temp = dto.DeckId1.ToString() + " - " + dto.DeckId2.ToString() + " - " + dto.DeckId3.ToString() + " - " + dto.DeckId4.ToString() + " - " + dto.DeckId5.ToString();
                        if (!IdRozdan.Contains(temp))
                            IdRozdan.Add(temp);
                    }
            }

            NotifyOfPropertyChange(() => IdRozdan);
        }

        public void ZmienWyniki(object sender)
        {
            int index = (int)sender;
            Wynik.NajlepszyCzas = 0;
            Wynik.NajlepszaIloscKlikniec = 0;
            Wynik.SredniCzas = 0;
            Wynik.SredniaIloscKlikniec = 0;

            if (count == 1)
            {
                if (Decks1 != null && Decks1.Count > 0)
                {
                    Wynik.NajlepszyCzas = Decks1.Min(i => i.Time);
                    Wynik.NajlepszaIloscKlikniec = (int)Decks1.Min(i => i.ClickNumber);
                    Wynik.SredniCzas = (int)Decks1.Average(i => i.Time);
                    Wynik.SredniaIloscKlikniec = (int)Decks1.Average(i => i.ClickNumber);
                }
            }
            else if (count == 3)
            {
                if (Decks3 != null && Decks3.Count > 0)
                {
                    Wynik.NajlepszyCzas = Decks3.Min(i => i.Time);
                    Wynik.NajlepszaIloscKlikniec = (int)Decks3.Min(i => i.ClickNumber);
                    Wynik.SredniCzas = (int)Decks3.Average(i => i.Time);
                    Wynik.SredniaIloscKlikniec = (int)Decks3.Average(i => i.ClickNumber);
                }
            }
            else
            {
                if (Decks5 != null && Decks5.Count > 0)
                {
                    Wynik.NajlepszyCzas = Decks5.Min(i => i.Time);
                    Wynik.NajlepszaIloscKlikniec = (int)Decks5.Min(i => i.ClickNumber);
                    Wynik.SredniCzas = (int)Decks5.Average(i => i.Time);
                    Wynik.SredniaIloscKlikniec = (int)Decks5.Average(i => i.ClickNumber);
                }
            }
        }
    }
}
