﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Runtime.Serialization;

namespace Freecell.Common
{
    [DataContract]
    public class DeckDto : Dto
    {
        [DataMember]
        public int Difficulty { get; set; }
        [DataMember]
        public int TimeLimit { get; set; }
        [DataMember]
        public int ClickLimit { get; set; }
    }
}
