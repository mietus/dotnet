﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Freecell.Common
{
    [DataContract]
    public class DeckRangeDto : Dto
    {
        [DataMember]
        public int From { get; set; }
        [DataMember]
        public int To { get; set; }

         public override string ToString()
        {
            if (From == To)
                return From.ToString();
            return From + " - " + To;
        }
    }
}
