﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Freecell.Common
{
    [DataContract]
    abstract public class Dto
    {
        [DataMember]
        public int Id { get; set; }
    }
}
