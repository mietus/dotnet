﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Freecell.Common
{
    [DataContract]
    public class ResultDto : Dto
    {
        [DataMember]
        public int DeckNumber { get; set; }
        [DataMember]
        public int Result { get; set; }
        [DataMember]
        public int Time { get; set; }
        [DataMember]
        public int ClickNumber { get; set; }
    }
}
