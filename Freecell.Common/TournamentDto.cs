﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Freecell.Common
{
    [DataContract]
    public class TournamentDto : Dto
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int FounderId { get; set; }

        [DataMember]
        public DateTime BeginingDate { get; set; }

        [DataMember]
        public DateTime EndingDate { get; set; }

        [DataMember]
        public int NumberDecks { get; set; }

        [DataMember]
        public int EntryFee { get; set; }

        [DataMember]
        public int Seed { get; set; }

        [DataMember]
        public int WinnerId { get; set; }
    }
}
