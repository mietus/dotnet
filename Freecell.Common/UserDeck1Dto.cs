﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Freecell.Common
{
    [DataContract]
    public class UserDeck1Dto : Dto
    {
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int DeckId1 { get; set; }
        [DataMember]
        public System.DateTime GameDate { get; set; }
        [DataMember]
        public int? Result { get; set; }
        [DataMember]
        public int? ClickNumber { get; set; }
        [DataMember]
        public int Time { get; set; }

    }
}
