﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Freecell.Common
{
    [DataContract]
    public class UserDeck3Dto : UserDeck1Dto
    {
        [DataMember]
        public int DeckId2 { get; set; }
        [DataMember]
        public int DeckId3 { get; set; }
    }
}
