﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Freecell.Common
{
    [DataContract]
    public class UserDeck5Dto : UserDeck3Dto
    {
        [DataMember]
        public int DeckId4 { get; set; }
        [DataMember]
        public int DeckId5 { get; set; }
    }
}
