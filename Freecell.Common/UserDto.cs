﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Freecell.Common
{
    [DataContract]
    public class UserDto : Dto
    {
        [DataMember]
        public string Nick { get; set; }
        [DataMember]
        public int Ranking { get; set; }
        [DataMember]
        public int GamesWon { get; set; }
        [DataMember]
        public int GamesPlayed { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public int Currency { get; set; }
         [DataMember]
        public int Admin { get; set; }
    }
}
