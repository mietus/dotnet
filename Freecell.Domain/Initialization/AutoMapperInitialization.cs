﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Freecell.Common;
using Freecell.Domain.Models;

namespace Freecell.Domain.Initialization
{
    public class AutoMapperInitialization
    {
        static private bool initialized = false;

        static public void Initialize()
        {
            if (!initialized)
            {
                Mapper.CreateMap<User, UserDto>();
                Mapper.CreateMap<UserDto, User>();

                Mapper.CreateMap<User_Deck1, UserDeck1Dto>();
                Mapper.CreateMap<UserDeck1Dto, User_Deck1>();

                Mapper.CreateMap<User_Deck3, UserDeck3Dto>();
                Mapper.CreateMap<UserDeck3Dto, User_Deck3>();

                Mapper.CreateMap<User_Deck5, UserDeck5Dto>();
                Mapper.CreateMap<UserDeck5Dto, User_Deck5>();

                Mapper.CreateMap<Deck, DeckDto>();
                Mapper.CreateMap<DeckDto, Deck>();

                Mapper.CreateMap<DeckRange, DeckRangeDto>();
                Mapper.CreateMap<DeckRangeDto, DeckRange>();

                Mapper.CreateMap<Tournament, TournamentDto>();
                Mapper.CreateMap<TournamentDto, Tournament>();
            }
        }
    }
}
