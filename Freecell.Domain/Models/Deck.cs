using Freecell.Domain.Models;
using System;
using System.Collections.Generic;

namespace Freecell.Domain.Models
{
    public class Deck : EntityBase
    {
        public Deck()
        {
            this.User_Deck1 = new List<User_Deck1>();
            this.User_Deck3 = new List<User_Deck3>();
            this.User_Deck31 = new List<User_Deck3>();
            this.User_Deck32 = new List<User_Deck3>();
            this.User_Deck5 = new List<User_Deck5>();
            this.User_Deck51 = new List<User_Deck5>();
            this.User_Deck52 = new List<User_Deck5>();
            this.User_Deck53 = new List<User_Deck5>();
            this.User_Deck54 = new List<User_Deck5>();
        }

        public int Difficulty { get; set; }
        public int TimeLimit { get; set; }
        public Nullable<int> ClickLimit { get; set; }
        public virtual ICollection<User_Deck1> User_Deck1 { get; set; }
        public virtual ICollection<User_Deck3> User_Deck3 { get; set; }
        public virtual ICollection<User_Deck3> User_Deck31 { get; set; }
        public virtual ICollection<User_Deck3> User_Deck32 { get; set; }
        public virtual ICollection<User_Deck5> User_Deck5 { get; set; }
        public virtual ICollection<User_Deck5> User_Deck51 { get; set; }
        public virtual ICollection<User_Deck5> User_Deck52 { get; set; }
        public virtual ICollection<User_Deck5> User_Deck53 { get; set; }
        public virtual ICollection<User_Deck5> User_Deck54 { get; set; }
    }
}
