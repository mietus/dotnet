using System;
using System.Collections.Generic;

namespace Freecell.Domain.Models
{
    public class DeckRange : EntityBase
    {
        public int From { get; set; }
        public int To { get; set; }
    }
}
