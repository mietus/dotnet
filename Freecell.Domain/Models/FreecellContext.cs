using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Freecell.Domain.Models.Mapping;
using System;

namespace Freecell.Domain.Models
{
    public class FreecellContext : DbContext,IFreecellContext
    {
        static FreecellContext()
        {
    Database.SetInitializer<FreecellContext>(null);

        }

		public FreecellContext()
			: base("Name=freecellContext")
		{ 
		}

        public IDbSet<Deck> Decks { get; set; }
        public IDbSet<DeckRange> DeckRanges { get; set; }
        public IDbSet<Tournament> Tournaments { get; set; }
        public IDbSet<User> Users { get; set; }
        public IDbSet<User_Deck1> User_Deck1 { get; set; }
        public IDbSet<User_Deck3> User_Deck3 { get; set; }
        public IDbSet<User_Deck5> User_Deck5 { get; set; }
        public IDbSet<User_Tournament> User_Tournament { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DeckMap());
            modelBuilder.Configurations.Add(new DeckRangeMap());
            modelBuilder.Configurations.Add(new TournamentMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new User_Deck1Map());
            modelBuilder.Configurations.Add(new User_Deck3Map());
            modelBuilder.Configurations.Add(new User_Deck5Map());
            modelBuilder.Configurations.Add(new User_TournamentMap());
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : EntityBase
        {
            return base.Set<TEntity>();
        }

        public new void SaveChanges()
        {
            base.SaveChanges();
        }
    }
}
