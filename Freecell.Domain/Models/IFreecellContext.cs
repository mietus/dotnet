﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Freecell.Domain.Models;

namespace Freecell.Domain.Models
{
    public interface IFreecellContext : IDisposable
    {
        IDbSet<Deck> Decks { get; set; }
        IDbSet<User_Tournament> User_Tournament { get; set; }
        IDbSet<Tournament> Tournaments { get; set; }
        IDbSet<User> Users { get; set; }
        IDbSet<DeckRange> DeckRanges { get; set; }
        IDbSet<User_Deck1> User_Deck1 { get; set; }
        IDbSet<User_Deck3> User_Deck3 { get; set; }
        IDbSet<User_Deck5> User_Deck5 { get; set; }
        
 

        IDbSet<TEntity> Set<TEntity>() where TEntity : EntityBase;

        void SaveChanges();
    }
}
