using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Freecell.Domain.Models.Mapping
{
    public class DeckMap : EntityTypeConfiguration<Deck>
    {
        public DeckMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("Deck");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Difficulty).HasColumnName("Difficulty");
            this.Property(t => t.TimeLimit).HasColumnName("TimeLimit");
            this.Property(t => t.ClickLimit).HasColumnName("ClickLimit");
        }
    }
}
