using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Freecell.Domain.Models.Mapping
{
    public class TournamentMap : EntityTypeConfiguration<Tournament>
    {
        public TournamentMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Tournament");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.FounderId).HasColumnName("FounderId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.BeginningDate).HasColumnName("BeginningDate");
            this.Property(t => t.EndingDate).HasColumnName("EndingDate");
            this.Property(t => t.NumberDecks).HasColumnName("NumberDecks");
            this.Property(t => t.EntryFee).HasColumnName("EntryFee");
            this.Property(t => t.Seed).HasColumnName("Seed");
            this.Property(t => t.WinnerId).HasColumnName("WinnerId");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.Tournaments)
                .HasForeignKey(d => d.FounderId);

        }
    }
}
