using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Freecell.Domain.Models.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Nick)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(30);

            this.Property(t => t.Password)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(95);

            // Table & Column Mappings
            this.ToTable("User");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Nick).HasColumnName("Nick");
            this.Property(t => t.Ranking).HasColumnName("Ranking");
            this.Property(t => t.GamesWon).HasColumnName("GamesWon");
            this.Property(t => t.GamesPlayed).HasColumnName("GamesPlayed");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.Currency).HasColumnName("Currency");
            this.Property(t => t.Admin).HasColumnName("Admin");
        }
    }
}
