using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Freecell.Domain.Models.Mapping
{
    public class User_Deck1Map : EntityTypeConfiguration<User_Deck1>
    {
        public User_Deck1Map()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("User_Deck1");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.DeckId1).HasColumnName("DeckId1");
            this.Property(t => t.GameDate).HasColumnName("GameDate");
            this.Property(t => t.Result).HasColumnName("Result");
            this.Property(t => t.ClickNumber).HasColumnName("ClickNumber");
            this.Property(t => t.Time).HasColumnName("Time");
            this.Property(t => t.LastCards).HasColumnName("LastCards");

            // Relationships
            this.HasRequired(t => t.Deck)
                .WithMany(t => t.User_Deck1)
                .HasForeignKey(d => d.DeckId1);
            this.HasRequired(t => t.User)
                .WithMany(t => t.User_Deck1)
                .HasForeignKey(d => d.UserId);

        }
    }
}
