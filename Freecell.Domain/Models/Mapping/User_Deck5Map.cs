using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Freecell.Domain.Models.Mapping
{
    public class User_Deck5Map : EntityTypeConfiguration<User_Deck5>
    {
        public User_Deck5Map()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("User_Deck5");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.DeckId1).HasColumnName("DeckId1");
            this.Property(t => t.DeckId2).HasColumnName("DeckId2");
            this.Property(t => t.DeckId3).HasColumnName("DeckId3");
            this.Property(t => t.DeckId4).HasColumnName("DeckId4");
            this.Property(t => t.DeckId5).HasColumnName("DeckId5");
            this.Property(t => t.GameDate).HasColumnName("GameDate");
            this.Property(t => t.Result).HasColumnName("Result");
            this.Property(t => t.ClickNumber).HasColumnName("ClickNumber");
            this.Property(t => t.Time).HasColumnName("Time");
            this.Property(t => t.LastCards).HasColumnName("LastCards");

            // Relationships
            this.HasRequired(t => t.Deck)
                .WithMany(t => t.User_Deck5)
                .HasForeignKey(d => d.DeckId1);
            this.HasRequired(t => t.Deck1)
                .WithMany(t => t.User_Deck51)
                .HasForeignKey(d => d.DeckId2);
            this.HasRequired(t => t.Deck2)
                .WithMany(t => t.User_Deck52)
                .HasForeignKey(d => d.DeckId3);
            this.HasRequired(t => t.Deck3)
                .WithMany(t => t.User_Deck53)
                .HasForeignKey(d => d.DeckId4);
            this.HasRequired(t => t.Deck4)
                .WithMany(t => t.User_Deck54)
                .HasForeignKey(d => d.DeckId5);
            this.HasRequired(t => t.User)
                .WithMany(t => t.User_Deck5)
                .HasForeignKey(d => d.UserId);

        }
    }
}
