using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Freecell.Domain.Models.Mapping
{
    public class User_TournamentMap : EntityTypeConfiguration<User_Tournament>
    {
        public User_TournamentMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("User_Tournament");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.TournamentId).HasColumnName("TournamentId");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.User_Tournament)
                .HasForeignKey(d => d.UserId);

        }
    }
}
