using System;
using System.Collections.Generic;

namespace Freecell.Domain.Models
{
    public class Tournament : EntityBase
    {
        public int FounderId { get; set; }
        public string Name { get; set; }
        public System.DateTime BeginningDate { get; set; }
        public System.DateTime EndingDate { get; set; }
        public int NumberDecks { get; set; }
        public int EntryFee { get; set; }
        public int Seed { get; set; }
        public Nullable<int> WinnerId { get; set; }
        public virtual User User { get; set; }
    }
}
