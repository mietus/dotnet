using System;
using System.Collections.Generic;

namespace Freecell.Domain.Models
{
    public class User : EntityBase
    {
        public User()
        {
            this.Tournaments = new List<Tournament>();
            this.User_Deck1 = new List<User_Deck1>();
            this.User_Deck3 = new List<User_Deck3>();
            this.User_Deck5 = new List<User_Deck5>();
            this.User_Tournament = new List<User_Tournament>();
        }

        public string Nick { get; set; }
        public int Ranking { get; set; }
        public int Admin { get; set; }
        public int GamesWon { get; set; }
        public int GamesPlayed { get; set; }
        public string Password { get; set; }
        public int Currency { get; set; }
        public virtual ICollection<Tournament> Tournaments { get; set; }
        public virtual ICollection<User_Deck1> User_Deck1 { get; set; }
        public virtual ICollection<User_Deck3> User_Deck3 { get; set; }
        public virtual ICollection<User_Deck5> User_Deck5 { get; set; }
        public virtual ICollection<User_Tournament> User_Tournament { get; set; }
    }
}
