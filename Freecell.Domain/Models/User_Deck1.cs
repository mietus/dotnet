using System;
using System.Collections.Generic;

namespace Freecell.Domain.Models
{
    public class User_Deck1 : EntityBase
    {
        public int UserId { get; set; }
        public int DeckId1 { get; set; }
        public System.DateTime GameDate { get; set; }
        public Nullable<int> Result { get; set; }
        public Nullable<int> ClickNumber { get; set; }
        public int Time { get; set; }
        public int LastCards { get; set; }
        public virtual Deck Deck { get; set; }
        public virtual User User { get; set; }
    }
}
