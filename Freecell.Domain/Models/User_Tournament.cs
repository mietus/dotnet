using System;
using System.Collections.Generic;

namespace Freecell.Domain.Models
{
    public class User_Tournament : EntityBase
    {
        public int UserId { get; set; }
        public int TournamentId { get; set; }
        public virtual User User { get; set; }
    }
}
