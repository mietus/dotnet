﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Freecell.Common;
using Freecell.Domain.Models;

namespace Freecell.Domain.Repos
{
    public class DeckRangeRepo : RepoBase, IDeckRangeRepo
    {
        public DeckRangeRepo(IFreecellContext context)
        {
            base._context = context;
        }

        public List<DeckRangeDto> GetDecksRanges()
        {
            return InTransaction(fc =>
            {
                List<DeckRangeDto> result = new List<DeckRangeDto>();
                var temp = fc.DeckRanges.ToList();
                if (temp != null)
                {
                    result = temp.Select(x => Mapper.Map<DeckRangeDto>(x)).ToList();
                }

                return result;
            });
        }

        public void AddDecksRanges(List<DeckRangeDto> decksRanges)
        {
            InTransaction(dc =>
            {
                foreach (DeckRangeDto z in decksRanges)
                    dc.DeckRanges.Add(Mapper.Map<DeckRange>(z));
            });
        }

        public void DeleteDecksRanges(List<DeckRangeDto> decksRanges)
        {
            InTransaction(dc =>
            {
                foreach (DeckRangeDto z in decksRanges)
                {
                    dc.DeckRanges.Remove(dc.DeckRanges.FirstOrDefault(i => i.Id == z.Id));
                }
            });
        }
    }
}
