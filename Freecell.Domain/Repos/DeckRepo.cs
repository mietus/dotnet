﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using System.Data.Entity;
using System.Threading;
using Freecell.Common;
using Freecell.Domain.Models;

namespace Freecell.Domain.Repos
{
    /// <summary>
    /// Klasa reprezentująca repozytorium dla encji typu Deck. 
    /// Zawiera metody pozwalające na asynchroniczny dostęp do encji typu Deck poprzez obiekty typu DeckDto.
    /// </summary>
    public class DeckRepo : RepoBase, IDeckRepo
    {
        public DeckRepo(IFreecellContext context)
        {
            base._context = context;
        }

        public DeckDto Get(long id)
        {
            return InTransaction(fc =>
            {
                DeckDto result = null;
                var temp = fc.Decks.FirstOrDefault(i => i.Id == id);
                if (temp != null)
                    result = Mapper.Map<DeckDto>(temp);

                return result;
            });
        }

        public void Insert(DeckDto Deck)
        {
            InTransaction(dc =>
            {
                dc.Decks.Add(Mapper.Map<Deck>(Deck));
            });
        }

        public DeckDto InsertAndGet(DeckDto Deck)
        {
            return InTransaction(dc =>
            {
                Deck u = Mapper.Map<Deck>(Deck);
                dc.Decks.Add(u);
                dc.SaveChanges();
                return Mapper.Map<DeckDto>(u);
            });
        }

        public void Update(DeckDto Deck)
        {
            InTransaction(dc =>
            {
                Deck toUpdate = dc.Decks.SingleOrDefault(i => i.Id == Deck.Id);
                Mapper.DynamicMap(Deck, toUpdate);
            });
        }

        public void Delete(long id)
        {
            InTransaction(dc =>
            {
                var temp = dc.Decks.FirstOrDefault(i => i.Id == id);
                if (temp != null)
                {
                    dc.Decks.Remove(temp);
                }
            });
        }
    }
}
