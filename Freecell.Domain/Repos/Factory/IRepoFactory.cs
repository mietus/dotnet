﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Freecell.Domain.Repos.Factory
{
    /// <summary>
    /// Interfejs service locatora.
    /// </summary>
    public interface IRepoFactory
    {
        T Get<T>() where T : class;
    }
}
