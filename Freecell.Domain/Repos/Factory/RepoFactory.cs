﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Freecell.Domain.Models;
using AutoMapper;

namespace Freecell.Domain.Repos.Factory
{
    /// <summary>
    /// Nasz własny service locator.
    /// </summary>
    public class RepoFactory : IRepoFactory
    {
        private DIContainer Container = new DIContainer();

        public RepoFactory()
        {
            Initialization.AutoMapperInitialization.Initialize();

            #region DIContainer mapping
            
            Container.Map<IUserRepo, UserRepo>(new FreecellContext());
            Container.Map<IDeckRepo, DeckRepo>(new FreecellContext());
            Container.Map<IUserDeckRepo, UserDeckRepo>(new FreecellContext());
            Container.Map<IDeckRangeRepo, DeckRangeRepo>(new FreecellContext());
            Container.Map<ITournamentRepo, TournamentRepo>(new FreecellContext());

            #endregion
        }

        public T Get<T>() where T : class
        {
            return Container.GetService<T>();
        }
    }
}
