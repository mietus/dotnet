﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Freecell.Common;

namespace Freecell.Domain.Repos
{
    public interface IDeckRangeRepo
    {
        List<DeckRangeDto> GetDecksRanges();

        void AddDecksRanges(List<DeckRangeDto> decksRanges);

        void DeleteDecksRanges(List<DeckRangeDto> decksRanges);
    }
}
