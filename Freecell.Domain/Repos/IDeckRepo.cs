﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Freecell.Common;

namespace Freecell.Domain.Repos
{
    /// <summary>
    /// Interfejs dla repozytorium encji typu User, na wypadek gdyby trzeba było tworzyć mocki repozytoriów.
    /// </summary>
    public interface IDeckRepo 
    {
  
        DeckDto Get(long id);

        void Insert(DeckDto rozdanie);

        DeckDto InsertAndGet(DeckDto user);

        void Update(DeckDto user);

        void Delete(long id);
    }
}
