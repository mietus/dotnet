﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Freecell.Common;

namespace Freecell.Domain.Repos
{
    /// <summary>
    /// Interfejs dla repozytorium encji typu User, na wypadek gdyby trzeba było tworzyć mocki repozytoriów.
    /// </summary>
    public interface ITournamentRepo
    {

        TournamentDto Get(int id);

        List<TournamentDto> GetTournaments();

        List<TournamentDto> GetAvailableTournaments();

        void Add(TournamentDto turniej);

        void Update(TournamentDto turniej);

        void Delete(int id);
    }
}
