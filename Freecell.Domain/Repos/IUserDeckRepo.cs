﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Freecell.Common;

namespace Freecell.Domain.Repos
{
    public interface IUserDeckRepo 
    {
        T Get<T>(int id) where T : UserDeck1Dto;

        UserDeck1Dto GetByUserId(int id);

        bool Insert<T>(T ur) where T : UserDeck1Dto;

        bool Update<T>(T ur) where T : UserDeck1Dto;

        int GetId(int userid, DateTime dateTime, int count);

        List<UserDeck1Dto> GetAllByUserId1(int userid);

        List<UserDeck3Dto> GetAllByUserId3(int userid);

        List<UserDeck5Dto> GetAllByUserId5(int userid);
    }
}
