﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Freecell.Common;

namespace Freecell.Domain.Repos
{
    /// <summary>
    /// Interfejs dla repozytorium encji typu User, na wypadek gdyby trzeba było tworzyć mocki repozytoriów.
    /// </summary>
    public interface IUserRepo
    {

        UserDto Get(long id);

        UserDto Get(string login);

        List<UserDto> GetUsers();

        void Insert(UserDto user);

        UserDto InsertAndGet(UserDto user);

        void Update(UserDto user);

        void Delete(long id);
    }
}
