﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Threading.Tasks;
using Freecell.Domain.Models;

namespace Freecell.Domain.Repos
{
    public abstract class RepoBase
    {
        protected IFreecellContext _context;
        private object _syncRoot = new object();


        protected T InTransaction<T>(Func<IFreecellContext, T> action)
        {
            lock (_syncRoot)
            {
                using (var tran = new TransactionScope())
                {
                    T result = action(_context);
                    _context.SaveChanges();
                    tran.Complete();

                    return result;
                }
            }
        }

        protected void InTransaction(Action<IFreecellContext> action)
        {
            lock (_syncRoot)
            {
                using (var tran = new TransactionScope())
                {                    
                    action(_context);
                    _context.SaveChanges();
                    tran.Complete();

                }
            }
        }

        protected Task<T> InTransactionTask<T>(Func<IFreecellContext, T> action)
        {
            return new Task<T>(i => InTransaction(action), null);
        }

    }
}
