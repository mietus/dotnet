﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Freecell.Common;
using Freecell.Domain.Models;

namespace Freecell.Domain.Repos
{
    public class TournamentRepo : RepoBase, ITournamentRepo
    {
        public TournamentRepo(IFreecellContext context)
        {
            base._context = context;
        }

        public TournamentDto Get(int id)
        {
            return InTransaction(fc =>
            {
                TournamentDto result = null;
                var temp = fc.Tournaments.FirstOrDefault(i => i.Id == id);
                if (temp != null)
                    result = Mapper.Map<TournamentDto>(temp);

                return result;
            });
        }

        public List<TournamentDto> GetTournaments()
        {
            return InTransaction(fc =>
            {
                List<TournamentDto> result = new List<TournamentDto>();
                foreach (Tournament t in fc.Tournaments)
                    result.Add(Mapper.Map<TournamentDto>(t));

                return result;
            });
        }

        public List<TournamentDto> GetAvailableTournaments()
        {
            throw new NotImplementedException();
        }

        public void Add(TournamentDto Tournament)
        {
            InTransaction(fc =>
            {
                fc.Tournaments.Add(Mapper.Map<Tournament>(Tournament));
            });
        }

        public void Update(TournamentDto user)
        {
            InTransaction(fc =>
            {
                //fc.Tournaments.(Mapper.Map<Tournament>(Tournament)); // tu cos
            });
        }

        public void Delete(int id)
        {
            InTransaction(fc =>
            {
                fc.Tournaments.Remove(fc.Tournaments.FirstOrDefault(i => i.Id == id)); 
            });
        }
    }
}
