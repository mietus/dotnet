﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Freecell.Common;
using Freecell.Domain.Models;

namespace Freecell.Domain.Repos
{
    public class UserDeckRepo : RepoBase, IUserDeckRepo
    {
        public UserDeckRepo(IFreecellContext context)
        {
            base._context = context;
        }

        public T Get<T>(int id) where T : UserDeck1Dto
        {
            return InTransaction(fc =>
            {
                T result = null;

                if (typeof(T) == typeof(UserDeck5Dto))
                {
                    var temp = fc.User_Deck5.FirstOrDefault(i => i.Id == id);
                    if (temp != null)
                        result = Mapper.Map<T>(temp);
                }
                else if (typeof(T) == typeof(UserDeck3Dto))
                {
                    var temp = fc.User_Deck3.FirstOrDefault(i => i.Id == id);
                    if (temp != null)
                        result = Mapper.Map<T>(temp);
                }
                else
                {
                    var temp = fc.User_Deck1.FirstOrDefault(i => i.Id == id);
                    if (temp != null)
                        result = Mapper.Map<T>(temp);
                }

                return result;
            });
        }

        public bool Insert<T>(T ur) where T : UserDeck1Dto
        {
            return InTransaction(fc =>
            {
                if (ur is UserDeck5Dto)
                {
                    fc.User_Deck5.Add(Mapper.Map<User_Deck5>(ur));
                    return true;
                }

                if (ur is UserDeck3Dto)
                {
                    fc.User_Deck3.Add(Mapper.Map<User_Deck3>(ur));
                    return true;
                }

                fc.User_Deck1.Add(Mapper.Map<User_Deck1>(ur));
                return true;
            });
        }

        public bool Update<T>(T ur) where T : UserDeck1Dto
        {
            return InTransaction(fc =>
            {
                if (ur is UserDeck5Dto)
                {
                    var temp = fc.User_Deck5.FirstOrDefault(i => i.Id == ur.Id);
                    temp.ClickNumber = ur.ClickNumber;
                    temp.Time = ur.Time;
                    temp.Result = ur.Result;
                    return true;
                }

                if (ur is UserDeck3Dto)
                {
                    var temp = fc.User_Deck3.FirstOrDefault(i => i.Id == ur.Id);
                    temp.ClickNumber = ur.ClickNumber;
                    temp.Time = ur.Time;
                    temp.Result = ur.Result;
                    return true;
                }

                var temp2 = fc.User_Deck1.FirstOrDefault(i => i.Id == ur.Id);
                temp2.ClickNumber = ur.ClickNumber;
                temp2.Time = ur.Time;
                temp2.Result = ur.Result;
                return true;
            });
        }

        public UserDeck1Dto GetByUserId(int userid)
        {
            return InTransaction(fc =>
            {
                UserDeck1Dto result = null;

                    var temp = fc.User_Deck5.FirstOrDefault(i => i.UserId == userid && i.Result == null);
                    if (temp != null)
                        result = Mapper.Map<UserDeck5Dto>(temp);
                    else
                {
                    var temp1 = fc.User_Deck3.FirstOrDefault(i => i.UserId == userid && i.Result == null);
                    if (temp1 != null)
                        result = Mapper.Map<UserDeck3Dto>(temp1);
                    else
                    {
                        var temp2 = fc.User_Deck1.FirstOrDefault(i => i.UserId == userid && i.Result == null);
                        if (temp2 != null)
                            result = Mapper.Map<UserDeck1Dto>(temp2);
                    }
                }
              

                return result;
            });
        }

        public List<UserDeck1Dto> GetAllByUserId1(int userid)
        {
            return InTransaction(fc =>
            {
                List<UserDeck1Dto> result = new List<UserDeck1Dto>();

                foreach (User_Deck1 ur in fc.User_Deck1)
                    if (ur.UserId == userid)
                        result.Add(Mapper.Map<UserDeck1Dto>(ur));

                return result;
            });
        }

        public List<UserDeck3Dto> GetAllByUserId3(int userid)
        {
            return InTransaction(fc =>
            {
                List<UserDeck3Dto> result = new List<UserDeck3Dto>();

                foreach (User_Deck3 ur in fc.User_Deck3)
                    if (ur.UserId == userid)
                        result.Add(Mapper.Map<UserDeck3Dto>(ur));

                return result;
            });
        }

        public List<UserDeck5Dto> GetAllByUserId5(int userid)
        {
            return InTransaction(fc =>
            {
                List<UserDeck5Dto> result = new List<UserDeck5Dto>();

                foreach (User_Deck5 ur in fc.User_Deck5)
                    if (ur.UserId == userid)
                        result.Add(Mapper.Map<UserDeck5Dto>(ur));

                return result;
            });
        }
        

        public int GetId(int userid, DateTime dateTime, int count)
        {
            return InTransaction(fc =>
            {
                int id = 0;

                if (count == 5)
                {
                    var temp = fc.User_Deck5.FirstOrDefault(i => i.UserId == userid && i.Result == null);
                    if (temp != null)
                        id = temp.Id;
                    
                }
                else if (count == 3)
                {
                    var temp = fc.User_Deck3.FirstOrDefault(i => i.UserId == userid && i.Result == null);
                    if (temp != null)
                        id = temp.Id;
                }
                else
                {
                    var temp = fc.User_Deck1.FirstOrDefault(i => i.UserId == userid && i.Result == null);
                    if (temp != null)
                        id = temp.Id;
                }

                return id;
            });
        }
    }
}
