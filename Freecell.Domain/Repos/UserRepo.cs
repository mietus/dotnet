﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using System.Data.Entity;
using System.Threading;
using Freecell.Common;
using Freecell.Domain.Models;

namespace Freecell.Domain.Repos
{
    /// <summary>
    /// Klasa reprezentująca repozytorium dla encji typu User. 
    /// Zawiera metody pozwalające na asynchroniczny dostęp do encji typu User poprzez obiekty typu UserDto.
    /// </summary>
    public class UserRepo : RepoBase, IUserRepo
    {
        public UserRepo(IFreecellContext context)
        {
            base._context = context;
        }

        public UserDto Get(long id)
        {
            return InTransaction(fc =>
            {
                UserDto result = null;
                var temp = fc.Users.FirstOrDefault(i => i.Id == id);
                if (temp != null)
                    result = Mapper.Map<UserDto>(temp);

                return result;
            });
        }


        public UserDto Get(string login)
        {
            return InTransaction<UserDto>(dc =>
            {
                UserDto result = null;
                var temp = dc.Users.FirstOrDefault(i => i.Nick == login);
                if (temp != null)
                    result = Mapper.Map<UserDto>(temp);

                return result;
            });
        }

        public List<UserDto> GetUsers()
        {
            return InTransaction<List<UserDto>>(dc =>
            {
                List<UserDto> result = new List<UserDto>();
                var temp = dc.Users.ToList();
                if (temp != null)
                {
                    result = temp.Select(x => Mapper.Map<UserDto>(x)).ToList();
                }

                return result;
            });
        }

        public void Insert(UserDto user)
        {
            InTransaction(dc =>
            {
                dc.Users.Add(Mapper.Map<User>(user));
            });
        }

        public UserDto InsertAndGet(UserDto user)
        {
            return InTransaction(dc =>
            {
                User u = Mapper.Map<User>(user);
                dc.Users.Add(u);
                dc.SaveChanges();
                return Mapper.Map<UserDto>(u);
            });
        }

        public void Update(UserDto user)
        {
            InTransaction(dc =>
            {
                User toUpdate = dc.Users.SingleOrDefault(i => i.Id == user.Id);
                Mapper.DynamicMap(user, toUpdate);
            });
        }

        public void Delete(long id)
        {
            InTransaction(dc =>
            {
                var temp = dc.Users.FirstOrDefault(i => i.Id == id);
                if (temp != null)
                {
                    foreach (var ur in dc.User_Deck1.Where(i => i.UserId == temp.Id))
                        dc.User_Deck1.Remove(ur);
                    foreach (var ur in dc.User_Deck3.Where(i => i.UserId == temp.Id))
                        dc.User_Deck3.Remove(ur);
                    foreach (var ur in dc.User_Deck5.Where(i => i.UserId == temp.Id))
                        dc.User_Deck5.Remove(ur);
                    dc.Users.Remove(temp);
                }
            });
        }
    }
}
