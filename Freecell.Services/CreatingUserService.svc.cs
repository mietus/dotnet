﻿using Freecell.Common;
using Freecell.Domain.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Freecell.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CreatingUserService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CreatingUserService.svc or CreatingUserService.svc.cs at the Solution Explorer and start debugging.
    public class CreatingUserService : ICreatingUserService
    {
        public bool CreateUser(UserDto dto)
        {
            //try
            //{
            dto.Password = PasswordDigest.Digest(dto.Password);
            Service.Factory.Get<IUserRepo>().Insert(dto);
            //}
            //catch
            //{
            //    return false;
            //}

            return true;
        }
    }
}
