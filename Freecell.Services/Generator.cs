﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Freecell.Domain.Repos;
using Freecell.Domain.Repos.Factory;
using Freecell.Domain.Models;
using Freecell.Common;

namespace Freecell.Services
{
    public class Generator
    {
        private static Generator instance = new Generator();
        private Generator()
        {
            ranges = Service.Factory.Get<IDeckRangeRepo>().GetDecksRanges();
            foreach (DeckRangeDto zr in ranges)
                size += zr.Count();
        }

        internal static Generator Instance
        {
            get
            {
                return instance;
            }
        }
        private int size = 0;

        private List<DeckRangeDto> ranges;

        private Random random = new Random();

        public static int[] DrawDeck(int nofDecks, int seed)
        {
            return instance.drawDeck(nofDecks, seed);
        }

        public static int[] Generate(int number)
        {
            return generatorWrapper.generatorWrapper.generujRozdanie(number);
        }


        private int[] drawDeck(int nofDecks, int seed)
        {
            if (nofDecks > size)
                throw new Exception("za malo rozdan dostepnych aby wylosowac");
            random = new Random(seed);
            List<int> list = new List<int>(nofDecks);
            while (list.Count != nofDecks)
            {
                int n = random.Next(size);
                for (int i = 0; i < ranges.Count; i++)
                {
                    if (n < ranges[i].Count())
                    {
                        if (!list.Contains(ranges[i].Get(n)))
                            list.Add(ranges[i].Get(n));
                        break;
                    }
                    n -= ranges[i].Count();
                }
            }
            list.Sort();
            return list.ToArray();
        }
    }
    static class DeckRangeDtoExtender
    {
        public static int Get(this DeckRangeDto zr, int i)
        {
            return zr.From + i;
        }

        public static int Count(this DeckRangeDto zr)
        {
            return zr.To - zr.From + 1;

        }
    }
}