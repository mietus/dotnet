﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Freecell.Common;

namespace Freecell.Services
{
    [ServiceContract]
    public interface IService
    {       
        [OperationContract]
        bool DeleteUser(UserDto dto);

        [OperationContract]
        GameDto GetNewGame(int count, int seed, int userid);
   
        [OperationContract]
        GameDto PreviewNewGame(int count, int seed, int userid);

        [OperationContract]
        bool EndGame(ResultDto dto);

        [OperationContract]
        List<UserDto> GetUsers();

        [OperationContract]
        UserDto GetUser(string login);

        [OperationContract]
        bool AddDecksRanges(List<DeckRangeDto> decksRange);

        [OperationContract]
        bool DeleteDecksRanges(List<DeckRangeDto> decksRange);

        [OperationContract]
        List<DeckRangeDto> GetDecksRanges();

        [OperationContract]
        bool FinishUnfinishedGame(int userid);

        [OperationContract]
        List<UserDeck1Dto> GetUserGames1(int userid);

        [OperationContract]
        List<UserDeck3Dto> GetUserGames3(int userid);

        [OperationContract]
        List<UserDeck5Dto> GetUserGames5(int userid);

        [OperationContract]
        TournamentDto GetTournament(int id);

        [OperationContract]
        List<TournamentDto> GetTournaments();

        [OperationContract]
        List<TournamentDto> GetAvailableTournaments();

        [OperationContract]
        bool AddTournament(TournamentDto user);

        [OperationContract]
        bool UpdateTournament(TournamentDto user);

        [OperationContract]
        bool DeleteTournament(int id);
    }
}
