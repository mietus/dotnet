﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;


namespace Freecell.Services
{
    public class PasswordDigest
    {
        private static SHA256CryptoServiceProvider provider = new SHA256CryptoServiceProvider();
        public static string Digest(string password)
        {
            string hPassword = ComputeHash(password, provider);
            return hPassword;
        }

        private static string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);
            return BitConverter.ToString(hashedBytes);
        }
    }
}