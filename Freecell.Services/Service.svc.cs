﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Freecell.Common;
using Freecell.Domain.Repos;
using Freecell.Domain.Repos.Factory;
using Freecell.Domain.Models;

namespace Freecell.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service : IService
    {
        private const int TIME_LIMIT_DEFAULT = 300;
        private const int CLICK_LIMIT_DEFAULT = 200;
        private const int DIFFICULTY_DEFAULT = 300;

        private static IRepoFactory _factory;
        public static IRepoFactory Factory
        {
            get
            {
                if (_factory == null)
                    _factory = new RepoFactory();

                return _factory;
            }
        }

        public Service()
        { }

        public Service(IRepoFactory factory)
        {
            _factory = factory;
        }      

        public bool DeleteUser(UserDto dto)
        {
            try
            {
                Factory.Get<IUserRepo>().Delete(dto.Id);
            }
            catch
            {
                return false;
            }

            return true;
        }

        public List<UserDto> GetUsers()
        {
            List<UserDto> result = new List<UserDto>();
            
            result = Factory.Get<IUserRepo>().GetUsers();
            return result;
        }

        public UserDto GetUser(string nick)
        {            
            UserDto result = new UserDto();
            if (!isAuthorized(nick))
                return result;
            result = Factory.Get<IUserRepo>().Get(nick);
            return result;
        }

        public GameDto GetNewGame(int count, int seed, int userid)
        {
            GameDto gameDto = new GameDto();
            if (!isAuthorized(userid))
                return gameDto;
            gameDto.DecksId = new List<int>(Generator.DrawDeck(count, seed));

            List<int[]> list = new List<int[]>(count);
            for (int i = 0; i < count; i++)
            {
                list.Add( Generator.Generate(gameDto.DecksId[i]));
            }
            gameDto.Decks = list;

            for (int i = 0; i < count; i++)
            {
                DeckDto deckDto = Factory.Get<IDeckRepo>().Get(gameDto.DecksId[i]);

                if (deckDto == null)
                {
                    deckDto = new DeckDto() { Id = gameDto.DecksId[i], TimeLimit = TIME_LIMIT_DEFAULT, ClickLimit = CLICK_LIMIT_DEFAULT, Difficulty = DIFFICULTY_DEFAULT };
                    Factory.Get<IDeckRepo>().Insert(deckDto);
                }

                gameDto.TimeLimit += deckDto.TimeLimit;
                gameDto.ClickLimit += (int)deckDto.ClickLimit;
                gameDto.Difficulty += deckDto.Difficulty;
            }

            gameDto.Difficulty /= count;

            UserDeck1Dto ur;

            if (count == 1)
            {
                ur = new UserDeck1Dto();
            }
            else if (count == 3)
            {
                ur = new UserDeck3Dto();
            }
            else
            {
                ur = new UserDeck5Dto();
            }

            ur.Id = 0;
            ur.UserId = userid;
            ur.GameDate = new DateTime(DateTime.Now.Ticks);

            ur.DeckId1 = gameDto.DecksId[0];
            if (count  >1)
            {
                (ur as UserDeck3Dto).DeckId2 = gameDto.DecksId[1];
                (ur as UserDeck3Dto).DeckId3 = gameDto.DecksId[2];
            }
            if (count >3)
            {
                (ur as UserDeck5Dto).DeckId4 = gameDto.DecksId[3];
                (ur as UserDeck5Dto).DeckId5 = gameDto.DecksId[4];
            }

            Factory.Get<IUserDeckRepo>().Insert(ur);
            gameDto.Id = Factory.Get<IUserDeckRepo>().GetId(userid, ur.GameDate,count);

            return gameDto;
        }

        public GameDto PreviewNewGame(int count, int seed, int userid)
        {
            GameDto gameDto = new GameDto();
            gameDto.DecksId = new List<int>(Generator.DrawDeck(count, seed));

            for (int i = 0; i < count; i++)
            {
                DeckDto deckDto = Factory.Get<IDeckRepo>().Get(gameDto.DecksId[i]);

                if (deckDto == null)
                {
                    deckDto = new DeckDto() { Id = gameDto.DecksId[i], TimeLimit = TIME_LIMIT_DEFAULT, ClickLimit = CLICK_LIMIT_DEFAULT, Difficulty = DIFFICULTY_DEFAULT };
                }

                gameDto.TimeLimit += deckDto.TimeLimit;
                gameDto.ClickLimit += (int)deckDto.ClickLimit;
                gameDto.Difficulty += deckDto.Difficulty;
            }

            gameDto.Difficulty /= count;

            return gameDto;
        }

        public bool EndGame(ResultDto dto)
        {           
            UserDeck1Dto ur;
            if (dto.DeckNumber == 1)
            {
                ur = Factory.Get<IUserDeckRepo>().Get<UserDeck1Dto>(dto.Id);
            }
            else if (dto.DeckNumber == 3)
            {
                ur = Factory.Get<IUserDeckRepo>().Get<UserDeck3Dto>(dto.Id);
            }
            else
            {
                ur = Factory.Get<IUserDeckRepo>().Get<UserDeck5Dto>(dto.Id);
            }
            if (!isAuthorized(ur.UserId))
                return false;
            ur.ClickNumber = dto.ClickNumber;
            int time =(int) (DateTime.Now - ur.GameDate).TotalSeconds;
            if (time > ur.Time + 10)
            {
                ur.Result = -5000;//przegrana, zeby zapobiec oszustwu
                ur.Time = time;

            }
            else
            {
                ur.Result = dto.Result;
                ur.Time = dto.Time;
            }

            UserDto toUpdate = Factory.Get<IUserRepo>().Get(ur.UserId);
            toUpdate.GamesPlayed++;
            toUpdate.GamesWon += ur.Result > 0 ? 1 : 0;

            if (dto.DeckNumber == 1)
            {               
                Factory.Get<IUserDeckRepo>().Update<UserDeck1Dto>(ur);
            }
            else if (dto.DeckNumber == 3)
            {
                Factory.Get<IUserDeckRepo>().Update<UserDeck3Dto>(ur as UserDeck3Dto);
            }
            else
            {
                Factory.Get<IUserDeckRepo>().Update<UserDeck5Dto>(ur as UserDeck5Dto);
            }
            return true;
        }


        public bool AddDecksRanges(List<DeckRangeDto> decksRanges)
        {
            if (!isAdmin())
                return false;
            Factory.Get<IDeckRangeRepo>().AddDecksRanges(decksRanges);
            return true;
        }

        public bool DeleteDecksRanges(List<DeckRangeDto> decksRanges)
        {
            if (!isAdmin())
                return false;
            Factory.Get<IDeckRangeRepo>().DeleteDecksRanges(decksRanges);
            return true;
        }

        public List<DeckRangeDto> GetDecksRanges()
        {
            if (!isAdmin())
                return new List<DeckRangeDto>();
            return Factory.Get<IDeckRangeRepo>().GetDecksRanges();
        }


        public bool FinishUnfinishedGame(int userid)
        {
            if (!isAuthorized(userid))
                return false;
            UserDeck1Dto urd = Factory.Get<IUserDeckRepo>().GetByUserId(userid);
            if (urd == null)
                return false;

            urd.ClickNumber = 0;
            urd.Time = 0;
            urd.Result = -5000;
            
            Factory.Get<IUserDeckRepo>().Update(urd);
            return true;
        }

        public List<UserDeck1Dto> GetUserGames1(int userid)
        {
            if (!isAuthorized(userid))
                return new List<UserDeck1Dto>();
            return Factory.Get<IUserDeckRepo>().GetAllByUserId1(userid);
        }

        public List<UserDeck3Dto> GetUserGames3(int userid)
        {
            if (!isAuthorized(userid))
                return new List<UserDeck3Dto>();
            return Factory.Get<IUserDeckRepo>().GetAllByUserId3(userid);
        }

        public List<UserDeck5Dto> GetUserGames5(int userid)
        {
            if (!isAuthorized(userid))
                return new List<UserDeck5Dto>();
            return Factory.Get<IUserDeckRepo>().GetAllByUserId5(userid);
        }

        public TournamentDto GetTournament(int id)
        {
            return Factory.Get<ITournamentRepo>().Get(id);
        }

        public List<TournamentDto> GetTournaments()
        {
            return Factory.Get<ITournamentRepo>().GetTournaments();
        }

        public List<TournamentDto> GetAvailableTournaments()
        {
            return Factory.Get<ITournamentRepo>().GetAvailableTournaments();
        }

        public bool AddTournament(TournamentDto turniej)
        {
            Factory.Get<ITournamentRepo>().Add(turniej);
            return true;
        }

        public bool UpdateTournament(TournamentDto turniej)
        {
            Factory.Get<ITournamentRepo>().Update(turniej);
            return true;
        }

        public bool DeleteTournament(int id)
        {
            Factory.Get<ITournamentRepo>().Delete(id);
            return true;
        }

        private bool isAuthorized(string userName)
        {
            return ServiceSecurityContext.Current.PrimaryIdentity.Name.Equals(userName);
        }

        private bool isAuthorized(int userId)
        {
            UserDto dto= Factory.Get<IUserRepo>().Get(userId);
            return ServiceSecurityContext.Current.PrimaryIdentity.Name.Equals(dto.Nick.Trim());
        }

        private bool isAdmin()
        {
            UserDto dto = Factory.Get<IUserRepo>().Get(ServiceSecurityContext.Current.PrimaryIdentity.Name);
            if (dto.Admin == 0)
                return false;
            return true;
        }

        private bool isAdmin(int userId)
        {
            UserDto dto = Factory.Get<IUserRepo>().Get(userId);
            if (dto.Admin == 0)
                return false;
            return ServiceSecurityContext.Current.PrimaryIdentity.Name.Equals(dto.Nick.Trim());
        }

    }
}
