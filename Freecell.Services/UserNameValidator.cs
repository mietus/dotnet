﻿using Freecell.Common;
using Freecell.Domain.Repos;
using System;
using System.Collections.Generic;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Web;

namespace Freecell.Services
{
    public class UserNameValidator : UserNamePasswordValidator
    {

        public override void Validate(string userName, string password)
        {
            UserDto result = new UserDto();
            result = Service.Factory.Get<IUserRepo>().Get(userName);
            if (result == null)
                throw new SecurityTokenException();
            String hash = PasswordDigest.Digest(password);
            if (!hash.Equals(result.Password))
                throw new SecurityTokenException();
        }
    }
}