﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeCell.Domain.Data
{
    public interface ICurrentContextProvider
    {
        TransactionContext GetCurrentContext();

        void RegisterContext(TransactionContext context);
    }
}
