﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Test.ServiceReference1 {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CompositeType", Namespace="http://schemas.datacontract.org/2004/07/Services")]
    [System.SerializableAttribute()]
    public partial class CompositeType : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool BoolValueField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StringValueField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool BoolValue {
            get {
                return this.BoolValueField;
            }
            set {
                if ((this.BoolValueField.Equals(value) != true)) {
                    this.BoolValueField = value;
                    this.RaisePropertyChanged("BoolValue");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StringValue {
            get {
                return this.StringValueField;
            }
            set {
                if ((object.ReferenceEquals(this.StringValueField, value) != true)) {
                    this.StringValueField = value;
                    this.RaisePropertyChanged("StringValue");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.IService")]
    public interface IService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetData", ReplyAction="http://tempuri.org/IService/GetDataResponse")]
        string GetData(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetData", ReplyAction="http://tempuri.org/IService/GetDataResponse")]
        System.Threading.Tasks.Task<string> GetDataAsync(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetDataUsingDataContract", ReplyAction="http://tempuri.org/IService/GetDataUsingDataContractResponse")]
        Test.ServiceReference1.CompositeType GetDataUsingDataContract(Test.ServiceReference1.CompositeType composite);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetDataUsingDataContract", ReplyAction="http://tempuri.org/IService/GetDataUsingDataContractResponse")]
        System.Threading.Tasks.Task<Test.ServiceReference1.CompositeType> GetDataUsingDataContractAsync(Test.ServiceReference1.CompositeType composite);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/CreateUser", ReplyAction="http://tempuri.org/IService/CreateUserResponse")]
        bool CreateUser(Common.UserDto dto);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/CreateUser", ReplyAction="http://tempuri.org/IService/CreateUserResponse")]
        System.Threading.Tasks.Task<bool> CreateUserAsync(Common.UserDto dto);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/DeleteUser", ReplyAction="http://tempuri.org/IService/DeleteUserResponse")]
        bool DeleteUser(Common.UserDto dto);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/DeleteUser", ReplyAction="http://tempuri.org/IService/DeleteUserResponse")]
        System.Threading.Tasks.Task<bool> DeleteUserAsync(Common.UserDto dto);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetNewGame", ReplyAction="http://tempuri.org/IService/GetNewGameResponse")]
        Common.GraDto GetNewGame(int count, int seed, int userid);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetNewGame", ReplyAction="http://tempuri.org/IService/GetNewGameResponse")]
        System.Threading.Tasks.Task<Common.GraDto> GetNewGameAsync(int count, int seed, int userid);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/EndGame", ReplyAction="http://tempuri.org/IService/EndGameResponse")]
        bool EndGame(Common.ResultDto dto);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/EndGame", ReplyAction="http://tempuri.org/IService/EndGameResponse")]
        System.Threading.Tasks.Task<bool> EndGameAsync(Common.ResultDto dto);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetUsers", ReplyAction="http://tempuri.org/IService/GetUsersResponse")]
        Common.UserDto[] GetUsers();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService/GetUsers", ReplyAction="http://tempuri.org/IService/GetUsersResponse")]
        System.Threading.Tasks.Task<Common.UserDto[]> GetUsersAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IServiceChannel : Test.ServiceReference1.IService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ServiceClient : System.ServiceModel.ClientBase<Test.ServiceReference1.IService>, Test.ServiceReference1.IService {
        
        public ServiceClient() {
        }
        
        public ServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string GetData(int value) {
            return base.Channel.GetData(value);
        }
        
        public System.Threading.Tasks.Task<string> GetDataAsync(int value) {
            return base.Channel.GetDataAsync(value);
        }
        
        public Test.ServiceReference1.CompositeType GetDataUsingDataContract(Test.ServiceReference1.CompositeType composite) {
            return base.Channel.GetDataUsingDataContract(composite);
        }
        
        public System.Threading.Tasks.Task<Test.ServiceReference1.CompositeType> GetDataUsingDataContractAsync(Test.ServiceReference1.CompositeType composite) {
            return base.Channel.GetDataUsingDataContractAsync(composite);
        }
        
        public bool CreateUser(Common.UserDto dto) {
            return base.Channel.CreateUser(dto);
        }
        
        public System.Threading.Tasks.Task<bool> CreateUserAsync(Common.UserDto dto) {
            return base.Channel.CreateUserAsync(dto);
        }
        
        public bool DeleteUser(Common.UserDto dto) {
            return base.Channel.DeleteUser(dto);
        }
        
        public System.Threading.Tasks.Task<bool> DeleteUserAsync(Common.UserDto dto) {
            return base.Channel.DeleteUserAsync(dto);
        }
        
        public Common.GraDto GetNewGame(int count, int seed, int userid) {
            return base.Channel.GetNewGame(count, seed, userid);
        }
        
        public System.Threading.Tasks.Task<Common.GraDto> GetNewGameAsync(int count, int seed, int userid) {
            return base.Channel.GetNewGameAsync(count, seed, userid);
        }
        
        public bool EndGame(Common.ResultDto dto) {
            return base.Channel.EndGame(dto);
        }
        
        public System.Threading.Tasks.Task<bool> EndGameAsync(Common.ResultDto dto) {
            return base.Channel.EndGameAsync(dto);
        }
        
        public Common.UserDto[] GetUsers() {
            return base.Channel.GetUsers();
        }
        
        public System.Threading.Tasks.Task<Common.UserDto[]> GetUsersAsync() {
            return base.Channel.GetUsersAsync();
        }
    }
}
