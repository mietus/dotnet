﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FreeCell.Domain.Data
{
    public class ThreadContextProvider : ICurrentContextProvider
    {
        private readonly ThreadLocal<TransactionContext> _context = new ThreadLocal<TransactionContext>();

        public TransactionContext GetCurrentContext()
        {
            if (_context.Value != null && _context.Value.IsDisposed)
            {
                _context.Value = null;
            }
            return _context.Value;
        }

        public void RegisterContext(TransactionContext context)
        {
            _context.Value = context;
        }
    }
}
