﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeCell.Domain.Data
{
    public class TransactionContext : IDisposable
    {
        #region Static

        private static ICurrentContextProvider _currentContext = new ThreadContextProvider();

        public static TransactionContext Current
        {
            get { return _currentContext.GetCurrentContext(); }
        }

        #endregion

        public TransactionContext(FreeCellContext context)//, IWinLabsDbEntities entities)
        {
            Context = context;
            //Entities = entities;
        }

        //public IWinLabsDbEntities Entities { get; private set; }
        public FreeCellContext Context { get; private set; }
        public bool RequestRollback { get; set; }
        public bool IsDisposed { get; private set; }

        public void Dispose()
        {
            this.IsDisposed = true;
        }
    }
}
