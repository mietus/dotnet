﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Freecell.Domain.Models;
using System.Transactions;

namespace testyBazy
{
    [TestClass]
    public class DataAccessTests
    {
        [TestMethod]
        public void CreatingContextConnectsToDatabase()
        {
            using (var context = new FreecellContext())
            {
                Assert.IsTrue(context.ObjectContext().DatabaseExists());
            }
        }

        [TestMethod]
        public void UserCRUDOperationsWork()
        {
            using (var context = new FreecellContext())
            {
                using (new TransactionScope())
                {
                    var user = new User()
                    {
                        Nick = "mietus",
                        Password = "dobrebydleta",
                        Ranking = 21,
                        GryRozegrane = 1,
                        GryWygrane = 1,
                        WirtualnaWaluta = 22,
                    };


                    context.Users.Add(user);

                    context.SaveChanges();

                    Assert.IsTrue(user.Id > 0);

                    context.ObjectContext().DetachAll();

                    var userFromDb = context.Users.Find(user.Id);
                    Assert.AreEqual(user.Id, userFromDb.Id);
                    Assert.AreEqual(user.Nick, userFromDb.Nick.Trim());
                    Assert.AreEqual(user.Password, userFromDb.Password.Trim());

                    context.Users.Remove(userFromDb);
                    context.SaveChanges();

                    context.ObjectContext().DetachAll();

                    var user3 = context.Users.Find(userFromDb.Id);

                    Assert.IsNull(user3);
                }
            }
        }

        [TestMethod]
        public void ZakresRozdanCRUDOperationsWork()
        {
            using (var context = new FreecellContext())
            {
                using (new TransactionScope())
                {
                    var zakres = new ZakresRozdan()
                    {
                        From = 2,
                        To = 10

                    };

                    context.ZakresRozdans.Add(zakres);
                    context.SaveChanges();
                    Assert.IsTrue(zakres.Id > 0);
                    context.ObjectContext().DetachAll();
                    var zakresFromDb = context.ZakresRozdans.Find(zakres.Id);
                    Assert.AreEqual(zakres.Id, zakresFromDb.Id);
                    context.ZakresRozdans.Remove(zakresFromDb);
                    context.SaveChanges();

                    context.ObjectContext().DetachAll();

                    zakres = context.ZakresRozdans.Find(zakresFromDb.Id);

                    Assert.IsNull(zakres);
                }
            }
        }
    }
}

