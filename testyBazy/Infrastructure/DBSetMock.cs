﻿using Freecell.Domain.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace testyBazy.Infrastructure
{
    public class DbSetMock<TEntity> : IDbSet<TEntity> where TEntity : EntityBase
    {
        private ObservableCollection<TEntity> _data = new ObservableCollection<TEntity>();
        private IQueryable<TEntity> _queryable;
        private int _identity = 1;

        public DbSetMock()
        {
            _queryable = _data.AsQueryable();
        }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable) _data).GetEnumerator();
        }

        public TEntity Find(params object[] keyValues)
        {
            return _data.FirstOrDefault(e => e.Id == (int) keyValues[0]);
        }

        public TEntity Add(TEntity entity)
        {
            _data.Add(entity);
            if (entity.Id==0)
                entity.Id = _identity++;
            return entity;
        }

        TEntity IDbSet<TEntity>.Remove(TEntity entity)
        {
            _data.Remove(entity);
            return entity;
        }

        public TEntity Attach(TEntity entity)
        {
            if (!_data.Contains(entity))
            {
                _data.Add(entity);
            }
            return entity;
        }

        public TEntity Create()
        {
            throw new NotImplementedException();
        }

        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, TEntity
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<TEntity> Local
        {
            get { return _data; }
        }

        IEnumerator<TEntity> IEnumerable<TEntity>.GetEnumerator()
        {
            return _data.GetEnumerator();
        }

        public Expression Expression
        {
            get { return _queryable.Expression; }
        }
        public Type ElementType
        {
            get { return _queryable.ElementType; }
        }
        public IQueryProvider Provider
        {
            get { return _queryable.Provider; }
        }
    }
}
