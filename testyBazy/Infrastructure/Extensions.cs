﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Data;

namespace testyBazy
{
    static class Extensions
    {
        public static ObjectContext ObjectContext(this DbContext dbContext)
        {
            return ((IObjectContextAdapter) dbContext).ObjectContext;
        }

        public static void DetachAll(this ObjectContext objectContext)
        {
            foreach (var entry in objectContext.ObjectStateManager.GetObjectStateEntries(
                EntityState.Added | EntityState.Deleted | EntityState.Modified | EntityState.Unchanged))
            {
                if (entry.Entity != null)
                {
                    objectContext.Detach(entry.Entity);
                }
            }
        }
    }
}
