﻿using Freecell.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testyBazy.Infrastructure
{
    public class FreecellContextMock : IFreecellContext
    {
        public FreecellContextMock()
        {
            Decks = new DbSetMock<Rozdanie>();
            Turniejs = new DbSetMock<Turniej>();
            Users = new DbSetMock<User>();
            ZakresRozdans = new DbSetMock<ZakresRozdan>();
            User_Rozdanie1 = new DbSetMock<User_Rozdanie1>();
            User_Rozdanie3 = new DbSetMock<User_Rozdanie3>();
            User_Rozdanie5 = new DbSetMock<User_Rozdanie5>();

            addSomeData();
        }

        private void addSomeData()
        {
            Users.Add (new User() 
                {
                    Id=0,
                        Nick = "testowy",
                        Password = "aaa",
                        Ranking=21,
                        GryRozegrane=1,
                        GryWygrane=1,
                        WirtualnaWaluta=22,
                    });
            ZakresRozdans.Add(new ZakresRozdan()
            {
                From = 10,
                To = 15
            });
        }

    

        public IDbSet<Rozdanie> Decks
        {
            get; 
            set;
        }

        public IDbSet<Turniej> Turniejs
        {
            get;
            set;
        }

        public IDbSet<User> Users
        {
            get;
            set;
        }

        public IDbSet<ZakresRozdan> ZakresRozdans
        {
            get;
            set;
        }

        public IDbSet<User_Rozdanie1> User_Rozdanie1
        {
            get;
            set;
        }

        public IDbSet<User_Rozdanie3> User_Rozdanie3
        {
            get;
            set;
        }

        public IDbSet<User_Rozdanie5> User_Rozdanie5
        {
            get;
            set;
        }

        public IDbSet<TEntity> Set<TEntity>() where TEntity : EntityBase
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
        }

        public void Dispose()
        {
        }
    }
}
