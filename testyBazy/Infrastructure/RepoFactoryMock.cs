﻿using Freecell.Domain.Repos;
using Freecell.Domain.Repos.Factory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testyBazy.Infrastructure
{
    public class RepoFactoryMock:IRepoFactory
    {
        private DIContainer Container = new DIContainer();

        public RepoFactoryMock()
        {
            Container.Map<IUserRepo, UserRepo>(new FreecellContextMock());
            Container.Map<IDeckRepo, DeckRepo>(new FreecellContextMock());
            Container.Map<IUserDeckRepo, UserDeckRepo>(new FreecellContextMock());
            Container.Map<IDeckRangeRepo, DeckRangeRepo>(new FreecellContextMock());
        }

        public T Get<T>() where T : class
        {
            return Container.GetService<T>();
        }
    }

    internal class DIContainer
    {
        Dictionary<Type, object> _map;

        public DIContainer()
        {
            _map = new Dictionary<Type, object>();
        }

        /// <summary>
        /// Mapuje interfejs na implementację tego interfejsu, z opcjonalnymi argumentami.
        /// </summary>
        /// <typeparam name="TIn">Typ interfejsu</typeparam>
        /// <typeparam name="TOut">Typ implementującego obiektu</typeparam>
        /// <param name="args">Opcjonalne argumenty do utworzenia implementującego typu.</param>
        public void Map<TIn, TOut>(params object[] args)
        {
            if (!_map.ContainsKey(typeof(TIn)))
            {
                object instance = Activator.CreateInstance(typeof(TOut), args);
                _map[typeof(TIn)] = instance;
            }
        }

        /// <summary>
        /// Zwraca serwis implementujący interfejs T.
        /// </summary>
        /// <typeparam name="T">Typ interfejsu</typeparam>
        public T GetService<T>() where T : class
        {
            if (_map.ContainsKey(typeof(T)))
                return _map[typeof(T)] as T;
            else
                throw new ApplicationException("The type " + typeof(T).FullName + " is not registered in the container");
        }
    }
}
