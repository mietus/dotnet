﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Freecell.Domain.Repos.Factory;
using Freecell.Domain.Repos;
using testyBazy.Infrastructure;
using Freecell.Common;

namespace testyBazy
{
    [TestClass]
    public class RepositoriesTests
    {   

        public RepositoriesTests()
        {
            Freecell.Domain.Initialization.AutoMapperInitialization.Initialize();
        }    

        [TestMethod]
        public void UserRepoGet()
        {
            UserRepo ur = new UserRepo(new FreecellContextMock());

            var dto3 = ur.Get("testowy");
            Assert.AreEqual(dto3.Password, "aaa");
        }
        [TestMethod]
        public void UserRepoInsert()
        {
            UserRepo ur = new UserRepo(new FreecellContextMock());
            UserDto dto=new UserDto()
            {
                Id = 0,
                Nick = "mietus",
                Password = "dobrebydleta",
                Ranking=21,
                GamesPlayed=1,
                GamesWon=1,
                Currency=22,                
            };
            ur.Insert(dto);
            dto = ur.Get(dto.Nick);
            Assert.AreEqual("mietus", dto.Nick);
        }

        [TestMethod]
        public void UserRepoUpdateDelete()
        {
            UserRepo ur = new UserRepo(new FreecellContextMock());
            var dto2 = ur.Get("testowy");
            dto2.Nick = "dwaMietusy";
            ur.Update(dto2);

            var dto = ur.Get(dto2.Id);
            Assert.AreEqual(dto.Nick, dto2.Nick);

            ur.Delete(dto.Id);

            dto=ur.Get(dto.Id);

            Assert.IsNull(dto);

        }


    }
}
