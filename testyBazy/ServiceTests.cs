﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Freecell.Services;
using Freecell.Domain.Repos.Factory;
using Freecell.Common;
using testyBazy.Infrastructure;
using Freecell.Domain.Repos;

namespace testyBazy
{
    [TestClass]
    public class ServiceTests
    {
        Service service = new Service(new RepoFactoryMock());
        public ServiceTests()
        {
            Freecell.Domain.Initialization.AutoMapperInitialization.Initialize();
          
        }

        [TestMethod]
        public void GetCreateDeleteUser()
        {
            var user=service.GetUser("testowy");
            Assert.AreEqual(user.Password, "aaa");
            var user2 = new UserDto()
             {
                Id=0,
                Nick = "mietus",
                Password = "2mietusy",
                Ranking=21,
                GamesPlayed=1,
                GamesWon=1,
                Currency=22,
            };

            service.CreateUser(user2);
            user = service.GetUser("mietus");
            Assert.AreEqual(user.Password, user2.Password);
            service.DeleteUser(user);
            Assert.IsNull(service.GetUser(user.Nick));
        }

        [TestMethod]
        public void GetPreviewAndNewGame()
        {
            var user2 = new UserDto()
            {
                Id = 0,
                Nick = "mietus",
                Password = "2mietusy",
                Ranking = 21,
                GamesPlayed = 1,
                GamesWon = 1,
                Currency = 22,
            };
            service.CreateUser(user2);
            user2=service.GetUser("mietus");

            var game=service.PreviewNewGame(3, 1, user2.Id);
            Assert.IsNotNull(game);
            Assert.AreNotEqual(game.TimeLimit, 0);

            var newGame = service.GetNewGame(3, 1, user2.Id);
            Assert.IsNotNull(newGame);
            Assert.AreNotEqual(newGame.Id, 0);
            Assert.AreEqual(game.TimeLimit, newGame.TimeLimit);

            bool b=service.EndGame(new ResultDto()
            {
                Time = 3,
                Id = newGame.Id,
                ClickNumber = 3,
                DeckNumber = 3,
                Result = 200
            });

            Assert.IsTrue(b);
            var f = Service.Factory;
            var userRozdanie=f.Get<IUserDeckRepo>().Get<UserDeck3Dto>(newGame.Id);

            Assert.AreEqual(userRozdanie.ClickNumber, 3);
            Assert.AreEqual(userRozdanie.DeckId1, newGame.DecksId[0]);
            Assert.AreEqual(userRozdanie.UserId, user2.Id);

          
        }
    }
}
